package cn.dyc.util

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object SparkUtil {
  def getSparkSession(appName:String = "app",master:String = "local[*]",cnfMap:Map[String,String] = Map.empty[String,String] ):SparkSession = {

    val conf = new SparkConf()
    conf.setAll(cnfMap)

    SparkSession.builder()
      .config(conf)
      .appName(appName)
      .master(master)
      .getOrCreate()
  }
}
