package cn.dyc.util

import scala.collection.mutable.ListBuffer

/**
  * @date: 2019/9/6
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 用户行为与业务路径的匹配工具
  */
object TransactionRouteMatch {

  /**
    *
    * @param userActions 用户行为事件序列
    * @param transSteps  业务定义的路径序列
    * @return 所满足的步骤列表
    */
  def routeMatch(userActions: List[String], transSteps: List[String]): List[Int] = {
    val buffer = new ListBuffer[Int]

    var index = -1
    var flag = true
    for (i <- 0 until transSteps.size if flag) {
      index = userActions.indexOf(transSteps(i), index + 1)
      if(index != -1) {
        buffer += i+1
      }else{
        flag = false
      }
    }
    buffer.toList
  }


  /**
    * 测试代码
    *
    * @param args
    */
  def main(args: Array[String]): Unit = {

    val u = List("C", "D", "B", "D", "A", "C", "F")
    val t = List("A", "B", "C", "D")

    println(routeMatch(u, t))
  }

}
