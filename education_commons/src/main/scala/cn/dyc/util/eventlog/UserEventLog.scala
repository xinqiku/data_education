package cn.dyc.util.eventlog

case class UserEventLog(
                         var cookieid: String = "",

                         var account: String = "",

                         var imei: String = "",

                         var osName: String = "",

                         var osVer: String = "",

                         var resolution: String = "",

                         var androidId: String = "",

                         var manufacture: String = "",

                         var deviceId: String = "",

                         var appid: String = "",

                         var appVer: String = "",

                         var release_ch: String = "",

                         var promotion_ch: String = "",

                         var areacode: Long = 0L,

                         var longtitude: Double = 0D,

                         var latitude: Double = 0D,

                         var carrier: String = "",

                         var netType: String = "",


                         var ip: String = "",

                         var sessionId: String = "",

                         var logType: String = "",

                         var commit_time: Long = 0l,

                         var event: Map[String, String] = null,

                         var geoHash: String = "",

                         var province: String = "",

                         var city: String = "",

                         var district: String = "",

                         var gid: String = "",

                         var title_kwds:String = ""
                       )
