package cn.dyc.util.eventlog

import ch.hsr.geohash.GeoHash
import com.alibaba.fastjson.{JSON, JSONException, JSONObject}


object EventJson2Bean {
  def genEventBean(json: String, geoMap:collection.Map[String, (String, String, String)]): UserEventLog = {
    var userEventLog: UserEventLog = null
    try {
      val userlogObject: JSONObject = JSON.parseObject(json)
      userEventLog = UserEventLog()
      val uob: JSONObject = userlogObject.getJSONObject("u")
      if (uob != null) {

        userEventLog.cookieid = uob.getString("cookieid")
        userEventLog.account = uob.getString("account")
        val phoneob: JSONObject = uob.getJSONObject("phone")
        if (phoneob != null) {
          userEventLog.imei = uob.getJSONObject("phone").getString("imei")
          userEventLog.osName = uob.getJSONObject("phone").getString("osName")
          userEventLog.osVer = uob.getJSONObject("phone").getString("osVer")
          userEventLog.resolution = uob.getJSONObject("phone").getString("resolution")
          userEventLog.androidId = uob.getJSONObject("phone").getString("androidId")
          userEventLog.manufacture = uob.getJSONObject("phone").getString("manufacture")
          userEventLog.deviceId = uob.getJSONObject("phone").getString("deviceId")
        }
        val app: JSONObject = uob.getJSONObject("app")
        if (app != null) {

          userEventLog.appid = uob.getJSONObject("app").getString("appid")
          userEventLog.appVer = uob.getJSONObject("app").getString("appVer")
          userEventLog.release_ch = uob.getJSONObject("app").getString("release_ch")
          userEventLog.promotion_ch = uob.getJSONObject("app").getString("promotion_ch")
        }
        val loc: JSONObject = uob.getJSONObject("loc")
        if (loc != null) {

          userEventLog.areacode = uob.getJSONObject("loc").getLong("areacode")
          userEventLog.longtitude = uob.getJSONObject("loc").getDouble("longtitude")
          userEventLog.latitude = uob.getJSONObject("loc").getDouble("latitude")
          userEventLog.carrier = uob.getJSONObject("loc").getString("carrier")
          userEventLog.netType = uob.getJSONObject("loc").getString("netType")
          userEventLog.ip = uob.getJSONObject("loc").getString("ip")
          if(geoMap != null) {
            val geoHash: String = GeoHash.withCharacterPrecision(userEventLog.latitude, userEventLog.latitude,5).toBase32
            userEventLog.geoHash = geoHash
            val tuple: (String, String, String) = geoMap.getOrElse(geoHash, ("","",""))
            userEventLog.province = tuple._1
            userEventLog.city = tuple._2
            userEventLog.district = tuple._3
          }
        }
        userEventLog.sessionId = uob.getString("sessionId")
      }
      userEventLog.logType = userlogObject.getString("logType")
      userEventLog.commit_time = userlogObject.getLong("commit_time")
      import scala.collection.JavaConversions._
      import scala.collection.JavaConverters._
      userEventLog.event = userlogObject.getJSONObject("event").getInnerMap.asScala.map(t => (t._1, t._2.toString)).toMap

      //过滤无效数据
      if (userEventLog.event == null ||
        ((userEventLog.account == null || userEventLog.account == "") &&
          (userEventLog.cookieid == null || userEventLog.cookieid == "") &&
          (userEventLog.sessionId == null || userEventLog.sessionId == "") &&
          (userEventLog.imei == null || userEventLog.imei == "") &&
          (userEventLog.deviceId == null || userEventLog.deviceId == "") &&
          (userEventLog.androidId == null || userEventLog.androidId == "") &&
          (userEventLog.ip == null || userEventLog.ip == ""))
      ) {
        userEventLog = null
      }
    } catch {
      case e: Throwable => null
    }
    userEventLog
  }
}
