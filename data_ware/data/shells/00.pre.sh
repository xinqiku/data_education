#!/bin/bash

dt=`date -d'-1 day' +'%Y-%m-%d'`
if [ $1 ]
then
dt=$1
fi
echo ${dt}

spark-submit --class cn.dyc.eventlog.EventLogETL \
--master yarn  \
--deploy-mode cluster  \
--num-executors 2 \
--executor-memory 2G \
--executor-cores 2 \
--driver-memory 1G \
/root/jar/pre.jar   \
/dicts/area  /rawlogs/${dt}  /preout/eventlogout/${dt}
