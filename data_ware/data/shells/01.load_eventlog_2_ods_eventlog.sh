#!/bin/bash

#  author: hunter.d
#  date:2019-09-02
#  desc:将预处理后的日志数据导入数仓ods层表：ods_eventlog
#

HIVE_HOME=/usr/apps/hive-2.3.5


dt=`date -d'-1 day' +'%Y-%m-%d'`
if [ $1 ]
then
dt=$1
fi
echo "准备导入 ${dt} 日的预处理日志数据........."

$HIVE_HOME/bin/hive -e "load data inpath '/preout/eventlogout/${dt}' into table yiee.ods_eventlog partition(dt='${dt}')"


