/*
   用户访问间隔分布报表：ads_user_acc_interval
   @src ： dws_user_continue_act
   @dst:   ads_user_acc_interval
   计算逻辑：1.限定一个时间范围
   2.同一个人的两个相邻活跃区间之间的天数间隔（包括间隔0天），就是访问间隔
   3.统计各种间隔的出现次数，就得到结果
   4.求两个相邻活跃区间之间的天数差，需要跨行计算，得用窗口分析函数lag over
   或 lead over
*/
-- @dst 建表
create table ads_user_acc_interval(
range_start string,
range_end string,
interval_days int,  -- 间隔天数
interval_cnts int   -- 发生次数
)
stored as parquet;

/*
计算思路
*/
-- 假设数据如下
uid,start,end
a,     3,  6
a,     8,  9
a,     13, 16

-- 把下一行的continue_start提到上一行
uid,start,end,next_start
a,    3,   6,    8
a,    8,   9,    13
a,    13,  16,   null
--然后将end-start，就是隔0天的次数，  next_start-end 就是间隔天数，发生次数为1
a,map(0,6-3,8-6,1)
a,map(0,9-8,13-9,1)
a,map(0,16-13,null-16,1)

-- 然后把map字段炸裂
0,    6-3
8-6,  1
0,    9-8
13-9, 1
0,    16-13
null,1

-- 分步骤解析
--步骤1
with tmp as(
select
uid,
map(
0,
datediff(if(continue_end='9999-12-31','2019-06-16',continue_end),continue_start),
datediff(lead(continue_start,1) over(partition by uid order by continue_start) ,if(continue_end='9999-12-31','2019-06-16',continue_end)),
1
) as m
from 
dws_user_continue_act
where 
continue_end>=date_sub('2019-06-16',30)
)

/*
+------+--------------+
| uid  |      m       |
+------+--------------+
| a    | {0:6,3:1}    |
| a    | {0:3,2:1}    |
| a    | {0:2914846}  |
| b    | {0:8,4:1}    |
| b    | {0:2,4:1}    |
| b    | {0:2914840}  |
| c    | {0:5}        |
| d    | {0:2914840}  |
| x    | {0:1}        |
+------+--------------+
*/

-- 步骤2：
select
uid,t.interval_days,t.interval_cnts
from tmp
lateral view
explode(m) t as interval_days,interval_cnts

/*
+------+------------------+------------------+
| uid  | t.interval_days  | t.interval_cnts  |
+------+------------------+------------------+
| a    | 0                | 6                |
| a    | 3                | 1                |
| a    | 0                | 3                |
| a    | 2                | 1                |
| a    | 0                | 13               |
| a    | NULL             | 1                |
| b    | 0                | 8                |
| b    | 4                | 1                |
| b    | 0                | 2                |
| b    | 4                | 1                |
| b    | 0                | 7                |
| b    | NULL             | 1                |
| c    | 0                | 5                |
| c    | NULL             | 1                |
| d    | 0                | 7                |
| d    | NULL             | 1                |
| x    | 0                | 1                |
| x    | NULL             | 1                |
+------+------------------+------------------+

*/
-- 步骤3
select 
interval_days,
count(1) as interval_cnts

from 
(
select
uid,t.interval_days,t.interval_cnts
from tmp
lateral view
explode(m) t as interval_days,interval_cnts
) o
where o.interval_days is not null
group by interval_days
;



-- 完整代码：

with tmp as(
select
uid,
map(
0,
datediff(if(continue_end='9999-12-31','2019-06-16',continue_end),continue_start),
datediff(lead(continue_start,1) over(partition by uid order by continue_start) ,if(continue_end='9999-12-31','2019-06-16',continue_end)),
1
) as m
from 
dws_user_continue_act
where 
continue_end>=date_sub('2019-06-16',30)
)
select 
interval_days,
sum(interval_cnts) as interval_cnts

from 
(
select
uid,t.interval_days,t.interval_cnts
from tmp
lateral view
explode(m) t as interval_days,interval_cnts
) o
where o.interval_days is not null
group by interval_days
;













