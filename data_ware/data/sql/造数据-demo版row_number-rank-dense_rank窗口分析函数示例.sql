zs,语文,80
zs,数学,70
zs,英语,60
zs,化学,90
zs,历史,85
ls,语文,92
ls,数学,70
ls,英语,78
ls,化学,86
ls,历史,95
ww,语文,92
ww,数学,90
ww,英语,68
ww,化学,78
ww,历史,98
zl,语文,92
zl,数学,90
zl,英语,68
zl,化学,78
zl,历史,98

create table demo_score(
name string,
course string,
score int
)
row format delimited fields terminated by ',';

load data local inpath '/root/s.dat' into table demo_score;





select
name,
course,
score,
row_number() over(partition by course order by score desc) as rn,
rank() over(partition by course order by score desc) as rk,
dense_rank() over(partition by course order by score desc) as drk
from demo_score
;















