with

select
if(b.uid is null, a.uid, b.uid) uid,
if(a.uid is not null, a.first_login, b.dt) first_login,
if(a.uid is not null, a.continue_start, b.dt) continue_start,


--if((a.continue_end!='9999-12-31' && b.uid is null) || (), a.continue_end, )
case when (a.uid is null) then '9999-12-31'
when (a.continue_end='9999-12-31' and b.uid is null ) then '2019-06-09'
else a.continue_end
end as continue_end
from

yiee.demo_user_continue_act a
full join
yiee.demo_dau_dtl b
on a.uid = b.uid

union all

select
o1.uid,
o1.first_login,
o2.dt continue_start,
'9999-12-31' continue_end
from
    (select
    o.uid,
    o.first_login,
    o.continue_start,
    o.continue_end
    from
        (select
        uid,
        max(first_login) first_login,
        max(continue_start) continue_start,
        max(continue_end) continue_end
        from
        demo_user_continue_act
        group  by uid
        ) o where o.continue_end != '9999-12-31'

) o1
join demo_dau_dtl o2
on o1.uid = o2.uid

