/*
   用户活跃度分析报表：ads_user_continuous_act
   @src ： dws_user_continue_act
   @dst:ads_user_continuous_act
   计算逻辑：先过滤出在当日有活跃的用户 :continue_end='9999-12-31'
             然后拿当日日期-continue_start得到连续活跃的天数
			 然后对不同天数的人分别计数
*/
create table ads_user_continuous_act(
dt string,
continue_days int,
user_cnts int
)


insert into table ads_user_continuous_act
select
'2019-06-16' as dt,
datediff('2019-06-16',continue_start)+1 as continue_days,
count(1) as user_cnts
from dws_user_continue_act
where continue_end='9999-12-31'
group by datediff('2019-06-16',continue_start)+1
;
