/*
	事件分析：dwd层明细记录表
	@src : ods_eventlog 日志贴源表
	@dst ：dwd_event_dtl 事件明细表
	-- 逻辑： 抽取字段

*/
-- 建表
drop table if exists dwd_event_dtl;
create table dwd_event_dtl(
event_type string comment '事件名称',
uid string,
sessionid string,
location string comment '事件所发生的页面',
event_dest string comment '事件的标的',
event_value double comment '事件的值(比如一个商品的价格，一个评分分值)',
commit_time bigint comment '事件发生的时间戳'
)
partitioned by (dt string)
stored as parquet;

-- ETL计算
insert into table dwd_event_dtl partition(dt='2019-06-16')
select
logtype as event_type,
COALESCE(
 if(trim(account) ='',null,account),
 if(trim(imei) = '',null,imei),
 if(trim(androidid) = '',null,androidid),
 if(trim(deviceid) = '',null,deviceid),
 if(trim(cookieid) = '',null,cookieid)
) as uid,
sessionid,
event['url'] as location,
case logtype
 when 'pg_view' then event['url']
 when 'thumbup' then event['skuid']
 when 'favor' then event['skuid']
 when 'ad_show' then event['ad_id']
 when 'ad_click' then event['ad_id']
 when 'add_cart' then event['skuid']
 when 'commit_cart' then event['order_id']
 when 'rate' then event['skuid']
 else event['skuid']
 end as event_dest,
case logtype
 when 'add_cart' then event['price']  -- 如果是添加商品到购物车，value为商品的价格
 when 'rate' then event['score']  -- 如果是评分，value为评分的分值
 when 'commit_cart' then event['order_amt'] --如果是提交购物车，value为订单的总价格
 else null
 end as event_value, 
commit_time 
from  
ods_eventlog where dt='2019-06-16'
;