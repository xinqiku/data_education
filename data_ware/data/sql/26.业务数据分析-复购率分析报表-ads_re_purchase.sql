/*
    购买业务订单商拼明细表
	@src  订单商品表:ods_b2c_orders_goods
	@src  商品信息表:ods_b2c_goods
	@src  订单信息表:ods_b2c_orders
	@dst  dwd_purchase_dtl
*/

-- @dst dwd_purchase_dtl
create table dwd_purchase_dtl(
uid string,
category1 string,
category2 string,
category3 string,
goods_id string,
deal_price double,
discounts double
)
partitioned by (dt string)
stored as parquet;

insert into table dwd_purchase_dtl partition(dt='2019-06-16')
select
c.user_id uid,
b.first_cat category1,
b.second_cat category2,
b.third_cat category3,
a.goods_id goods_id,
a.goods_price * a.goods_amount deal_price,
b.curr_price * b.discount discounts
from
ods_b2c_orders_goods a
join ods_b2c_orders c
on a.create_time > '2019-06-15 23:59:59' and a.order_id = c.order_id
join
ods_b2c_goods b
on a.goods_id = b.goods_id


/**
最近30天内的复购率分析
@src : dwd_purchase_dtl
@dst : dws_repurchase
 */

create table ads_repurchase(
catagroy3 string,
users int,
user2s int,
onepurchase string,
userns int,
morepurchase string
)
stored as parquet;
1,三星,
1,三星,
1,三星,

with tmp as(
select
category3,
row_number() over(partition by uid,category3) rn
from
dwd_purchase_dtl
)
insert into table ads_repurchase
select
category3,
count(1) users,
count(if(rn=2,1,null)) user2s,
concat(100*count(if(rn=2,1,null))/count(1), '%') onepurchase,
count(if(rn>2,1,null)) userns,
concat(100*count(if(rn>2,1,null))/count(1), '%') morepurchase
from
tmp
group by category3