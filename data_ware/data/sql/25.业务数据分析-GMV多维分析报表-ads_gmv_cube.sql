/*
    GVM（成交总额）多维分析报表
	@src  订单商品表:ods_b2c_orders_goods
	@src  商品信息表:ods_b2c_goods
	@src  品牌信息表:ods_b2c_brand
	@dst  ads_gmv_cube
*/

-- @dst dwd_gmv
drop table if exists dwd_gmv_dtl;
create table dwd_gmv_dtl(
goods_amount bigint,
goods_price double,
category1 string,
category2 string,
category3 string,
brand string
)
partitioned by (dt string)
stored as parquet;

insert into table dwd_gmv_dtl partition(dt = '2019-06-16')
select
a.goods_amount,
a.goods_price,
b.first_cat_name as cat1,
b.second_cat_name as cat2,
b.third_cat_name as cat3,
c.name as brand
from ods_b2c_orders_goods a
join ods_b2c_goods  b  on a.create_time>'2019-06-15 23:59:59' and a.goods_id = b.goods_id
join ods_b2c_brand  c  on b.brand_id = c.id

-- @dst 建表
drop table if exists ads_gmv_cube;
create table ads_gmv_cube(
dt string,
brand string,
cat1  string,
cat2  string,
cat3  string,
order_amt double
)

stored as parquet
;


-- etl计算
insert into table ads_gmv_cube

select
'2019-06-16' dt,
brand,
category1,
category2,
category3,
sum(goods_amount*goods_price)
from dwd_gmv_dtl
group by brand,category1,category2,category3
with rollup
;






















