/*
   用户访问间隔分布报表：ads_user_acc_interval
   @src ： dws_user_continue_act
   @dst:   ads_user_acc_interval
   计算逻辑：1.限定一个时间范围
   2.同一个人的两个相邻活跃区间之间的天数间隔，就是访问间隔
   3.统计各种间隔的出现次数，就得到结果
   4.求两个相邻活跃区间之间的天数差，需要跨行计算，得用窗口分析函数lag over
   或 lead over
*/
-- @dst 建表
create table ads_user_acc_interval(
range_start string,
range_end string,
interval_days int,  -- 间隔天数
interval_cnts int   -- 发生次数
)
stored as parquet;


-- etl计算
with tmp as(
select
uid,
continue_end,
lead(continue_start,1) over(partition by uid order by continue_start) as next_start
from
dws_user_continue_act
where
continue_end>=date_sub('2019-06-16',30)
)

insert into table ads_user_acc_interval
select
date_sub('2019-06-16',30) as range_start,
'2019-06-16' as range_end,
datediff(next_start,continue_end)-1 as interval_days,
count(1) as interval_cnts
from tmp
where next_start is not null
group by datediff(next_start,continue_end)-1
;


with tmp as(
select
uid,
continue_start,
continue_end,
row_number() over(partition by uid order by continue_start) as rn
from
dws_user_continue_act
where continue_end>=date_sub('2019-06-16',30)
)


select
date_sub('2019-06-16',30) range_start,
'2019-06-16' range_end,
o.interval_days,
count(1) interval_cnts
from
(select
t1.uid,
datediff(t2.continue_start,t1.continue_end)-1 interval_days
from
tmp t1
join tmp t2
on t1.uid = t2.uid and t1.rn = t2.rn - 1
)o
where o.interval_days is not null
group by o.interval_days



