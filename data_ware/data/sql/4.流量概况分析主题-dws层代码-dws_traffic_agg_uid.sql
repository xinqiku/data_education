--创建数据汇总层-按uid 汇总的信息
drop table if exists yiee.dws_traffic_agg_uid;
CREATE EXTERNAL TABLE
  IF NOT EXISTS yiee.dws_traffic_agg_uid(
  uid String,
  session_cnts int,
  time_amt int,
  pv_cnts int,
  province string,
  city string,
  district string,
  manufacture string,
  osname string,
  osver string
)
  partitioned BY (dt String)
  stored AS parquet;

insert into table yiee.dws_traffic_agg_uid partition(dt = '2019-06-16')
select
uid ,
count(1) session_cnts,
sum(end_time - start_time) time_amt,
sum(pv_cnts) pv_cnts,
max(province) as province,
max(city) as city,
max(district) as district,
max(manufacture) as manufacture,
max(osname) as osname,
max(osver) as osver

from
yiee.dws_traffic_agg_session
where dt = '2019-06-16'
group by uid
