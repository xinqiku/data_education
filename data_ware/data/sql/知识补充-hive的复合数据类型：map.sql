hive中的map字段类型

1.基本操作
有如下数据：
session01,pg_view,url:/a|pre_url:/b|title:多易教育大数据培训
session02,pg_view,url:/c|pre_url:/x|title:多易教育历届优学毕业生英雄榜
session03,ad_click,url:/c|adid:ad001|adproduct:p002|title:多易教育历届优学毕业生英雄榜

load data local inpath '/root/demo_map.dat' into table demo_map;


需要在hive中建表映射：
drop table demo_map;
create table demo_map(
sessionid string,
eventtype string,
properties map<string,string>
)
row format delimited fields terminated by ','
collection items terminated by '|'
map keys terminated by ':'
;

load data local inpath '/root/demo_map.dat' into table demo_map;


2.map数据类型的各类运算函数
根据指定key，取value
select properties['url'] from demo_map;

求map字段的元素个数
select size(properties) from demo_map;

求map中的所有key、value
select map_keys(properties) from demo_map;  -- 返回的是一个数组
select map_values(properties) from demo_map; -- 返回的也是一个数组

求map中是否包含某个指定的key
select array_contains(map_keys(properties),'adid') from demo_map;

在sql中构造一个map
方式1：map(k1,v1,k2,v2,k3,v3)
select map(sessionid,eventtype),properties from demo_map;
+---------------------------+----------------------------------------------------------------------------------------+
|            _c0            |                     properties                                                         |
+---------------------------+----------------------------------------------------------------------------------------+
| {"session01":"pg_view"}   | {"url":"/a","pre_url":"/b","title":"多易教育大数据培训"}                               |
| {"session02":"pg_view"}   | {"url":"/c","pre_url":"/x","title":"多易教育历届优学毕业生英雄榜"}                     |
| {"session03":"ad_click"}  | {"url":"/c","adid":"ad001","adproduct":"p002","title":"多易教育历届优学毕业生英雄榜"}  |
+---------------------------+----------------------------------------------------------------------------------------+
方式2：str_to_map(str,sep1,sep2)



3.综合应用案例：
有如下表：留存明细表
+-------+------------+-------------------+-------------------+
| t.dt  | t.dnu_cnt  | t.retention_days  | t.retention_cnts  |
+-------+------------+-------------------+-------------------+
| 0601  | 100        | 1                 | 80                |
| 0601  | 100        | 2                 | 60                |
| 0601  | 100        | 3                 | 50                |
| 0601  | 100        | 4                 | 60                |
| 0601  | 100        | 5                 | 40                |
| 0602  | 80         | 1                 | 80                |
| 0602  | 80         | 2                 | 50                |
| 0602  | 80         | 3                 | 40                |
| 0602  | 80         | 4                 | 30                |
| 0603  | 120        | 1                 | 110               |
| 0603  | 120        | 2                 | 80                |
| 0603  | 120        | 3                 | 90                |
+-------+------------+-------------------+-------------------+

需要将其转成横表

0601  100   {1:80,2:60,3:50,4:60,5:40}

-- 思路： 先将每一行的留存天数和留存人数拼成：分隔的字符串
        -- 再按dt分组，将每一行的kv字符串收集到一个数组
		-- 再将这个kv数组拼成一个字符串
		-- 再将上面这个大字符串转成map结构
select
dt,
dnu_cnt,
str_to_map(concat_ws(',',collect_set(concat_ws(':',cast(retention_days as string),cast(retention_cnts as string)))),',',':') as info
from demo_retention_dtl
group by dt,dnu_cnt
;
+-------+----------+-------------------------------------------------+
|  dt   | dnu_cnt  |                      info                       |
+-------+----------+-------------------------------------------------+
| 0601  | 100      | {"1":"80","2":"60","3":"50","4":"60","5":"40"}  |
| 0602  | 80       | {"1":"80","2":"50","3":"40","4":"30"}           |
| 0603  | 120      | {"1":"110","2":"80","3":"90"}                   |
+-------+----------+-------------------------------------------------+

-- 根据上面的结果，可以得出页面所要的固定列的报表：

with tmp as(
select
dt,
dnu_cnt,
str_to_map(concat_ws(',',collect_set(concat_ws(':',cast(retention_days as string),cast(retention_cnts as string)))),',',':') as info
from demo_retention_dtl
group by dt,dnu_cnt
)


select
dt,
dnu_cnt,
info['1'] as ret_1,
info['2'] as ret_2,
info['3'] as ret_3,
info['4'] as ret_4,
info['5'] as ret_5,
info['6'] as ret_6,
info['7'] as ret_7,
info['14'] as ret_14,
info['30'] as ret_30

from  tmp
;
+-------+----------+--------+--------+--------+--------+--------+--------+--------+---------+---------+
|  dt   | dnu_cnt  | ret_1  | ret_2  | ret_3  | ret_4  | ret_5  | ret_6  | ret_7  | ret_14  | ret_30  |
+-------+----------+--------+--------+--------+--------+--------+--------+--------+---------+---------+
| 0601  | 100      | 80     | 60     | 50     | 60     | 40     | NULL   | NULL   | NULL    | NULL    |
| 0602  | 80       | 80     | 50     | 40     | 30     | NULL   | NULL   | NULL   | NULL    | NULL    |
| 0603  | 120      | 110    | 80     | 90     | NULL   | NULL   | NULL   | NULL   | NULL    | NULL    |
+-------+----------+--------+--------+--------+--------+--------+--------+--------+---------+---------+

4. 类似扩展案例
t_score:
a,语文,80
b,语文,80
c,语文,80
a,数学,80
a,英语,80
a,政治,80
b,数学,80
b,物理,80
d,语文,80
d,历史,80
a,化学,80
create table t_score (
uid string,
name string,
score int
)
row format delimited fields terminated by ','
load data local inpath '/root/data/t_score.dat' into table t_score;
==》
姓名    语文成绩    数学成绩     化学成绩     物理成绩    英语成绩     政治成绩     历史成绩
a          80         80           80            null        80            80         null     
with tmp as

(select
uid,
str_to_map(concat_ws(',',collect_set(concat_ws(':',name,cast(score as string)))),',',':') as info
from
t_score
group  by uid
)

select
uid,
info['语文'],
info['数学'],
info['化学'],
info['物理'],
info['英语'],
info['政治'],
info['历史']
from tmp;
































