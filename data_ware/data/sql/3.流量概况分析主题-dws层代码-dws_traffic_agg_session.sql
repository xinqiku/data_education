--创建数据汇总层-按uid session 汇总的信息
drop table if exists yiee.dws_traffic_agg_session;
CREATE EXTERNAL TABLE
  IF NOT EXISTS yiee.dws_traffic_agg_session(
  uid String,
  sessionid String,   --操作系统名称
  start_time bigint,
  end_time bigint,
  pv_cnts int,
  province string,
  city string,
  district string,
  manufacture string,
  osname string,
  osver string,
  release_ch string
)
  partitioned BY (dt String)
  stored AS parquet

insert into table yiee.dws_traffic_agg_session partition(dt='2019-06-16')

select
uid,
sessionId,
min(commit_time) start_time,
max(commit_time) end_time,
count(1) pv_cnts,
max(province),
max(city),
max(district),
max(manufacture),
max(osname),
max(osver),
max(release_ch)

from
yiee.dwd_traffic_dtl
where dt='2019-06-16'
group by uid, sessionId;