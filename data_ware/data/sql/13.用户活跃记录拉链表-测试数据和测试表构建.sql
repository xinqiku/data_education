create table dws_user_continue_act(
uid string,
first_login string,
continue_start string,
continue_end string
)
row format delimited fields terminated by ','
;

vi x.dat
a,2019-05-20,2019-05-20,2019-05-26
a,2019-05-20,2019-05-29,2019-06-01
a,2019-05-20,2019-06-03,9999-12-31
b,2019-05-22,2019-05-22,2019-05-30
b,2019-05-22,2019-06-03,2019-06-05
c,2019-06-03,2019-06-03,9999-12-31
x,2019-06-03,2019-06-03,2019-06-04

load data local inpath '/root/x.dat' into table dws_user_continue_act;


6.9号日活
create table demo_dau_dtl(
uid string
)
partitioned by (dt string)
row format delimited fields terminated by ',';

vi dau.dat
a
b
d
load data local inpath '/root/dau.dat' into table demo_dau_dtl partition(dt='2019-06-09')



