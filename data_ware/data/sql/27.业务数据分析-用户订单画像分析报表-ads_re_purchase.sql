-- 订单宽表 dm_b2c_orders  内涵订单主要新信息+订单收货人详细信息+代金券信息
-- 来源于  订单主要信息表 + 订单收货人详情信息

drop table if exists dws_b2c_orders;
create table dws_b2c_orders(
order_id              bigint       ,-- 订单id
order_no              string       ,-- 订单号
order_date            string       ,-- 订单日期
user_id               bigint       ,-- 用户ID
user_name             string       ,-- 登录名
order_money           double       ,-- 订单金额
order_type            string       ,-- 订单类型
order_status          string       ,-- 订单状态
pay_type              string       ,-- 支付类型
pay_status            string       ,-- 支付状态
order_source          string       ,-- 订单来源
consignee             string       ,-- 收货人姓名
area_id               bigint       ,-- 收货人地址ID
area_name             string       ,-- 地址ID对应地址段
address               string       ,-- 收货人地址
mobilephone           string       ,-- 收货人手机号
telphone              string       ,-- 收货人电话
coupon_id             bigint       ,-- 代金券ID
coupon_money          double       ,-- 代金券金额
carriage_money        double       ,-- 运费
create_time           string       ,-- 创建时间
last_update_time      string       ,-- 最后修改时间
dw_date               string
)


-- ods_b2c_orders
-- join
-- ods_b2c_orders_desc


-- 订单指标表 dm_user_order_tag
drop table if exists ads_user_order_tag;
create table ads_user_order_tag(
user_id                        bigint     ,--用户
first_order_time               string     ,--第一次消费时间    --这个人的第一个订单时间
last_order_time                string     ,--最近一次消费时间
first_order_ago                bigint     ,--首单距今时间
last_order_ago                 bigint     ,--尾单距今时间
month1_order_cnt               bigint     ,--近30天购买次数
month1_order_amt               double     ,--近30天购买金额
month2_order_cnt               bigint     ,--近60天购买次数
month2_order_amt               double     ,--近60天购买金额
month3_order_cnt               bigint     ,--近90天购买次数
month3_order_amt               double     ,--近90天购买金额
max_order_amt                  double     ,--最大消费金额
min_order_amt                  double     ,--最小消费金额
total_order_cnt                bigint     ,--累计消费次数（不含退拒）
total_order_amt                double     ,--累计消费金额（不含退拒）
total_coupon_amt               double     ,--累计使用代金券金额
user_avg_amt                   double     ,--客单价（含退拒）
month3_user_avg_amt            double     ,--近90天客单价（含退拒）
common_address                 string     ,--常用收货地址
common_paytype                 string     ,--常用支付方式
month1_cart_cnt                bigint     ,--最近30天添加购物车次数
month1_cart_goods_cnt          bigint     ,--最近30天添加购物车商品总件数
month1_cart_submit_cnt         bigint     ,--最近30天提交商品件数
month1_cart_submit_rate        double     ,--最近30天提交的商品的比率
month1_cart_cancel_cnt         bigint     ,--最近30天商品取消数量
month1_cart_cancel_rate        double     ,--最近30天商品取消比率
dw_date                        string     ,--数仓计算日期
) partitioned by
(dt string)
;


-- etl 计算

with t1 as (

select
user_id,
min(create_time) as first_order_time, -- 首单时间
max(create_time) as last_order_time, --尾单时间
-- first_order_ago
-- last_order_ago
count(if(datediff('2019-06-16',create_time)<=30,1,null)) as month1_order_cnt,-- 30天内的订单总数
sum(if(datediff('2019-06-16',create_time)<=30,order_money,0)) as month1_order_amt, --30天内的订单总额

count(if(datediff('2019-06-16',create_time)<=60,1,null)) as month2_order_cnt,-- 60天内的订单总数
sum(if(datediff('2019-06-16',create_time)<=60,order_money,0)) as month2_order_amt, --60天内的订单总额

count(if(datediff('2019-06-16',create_time)<=90,1,null)) as month3_order_cnt,-- 90天内的订单总数
sum(if(datediff('2019-06-16',create_time)<=90,order_money,0)) as month3_order_amt, --90天内的订单总额

max(order_money) as max_order_amt, -- 最大消费金额
min(order_money) as min_order_amt, -- 最小消费金额

count(if(order_status in ('退货','拒收'),null,1)) as p_total_order_cnt, -- 累计订单总数（不含退拒）
sum(if(order_status in ('退货','拒收'),0,order_money)) as p_total_order_amt, -- 累计消费总额（不含退拒）

sum(coupon_money) as total_coupon_amt, -- 累计使用代金券总额

count(1) as total_order_cnt, -- 累计订单总数（含退拒）
sum(order_money) as total_order_amt -- 累计消费总额（含退拒）
-- user_avg_amt 客单价（含退拒）
-- month3_user_avg_amt --90天内的客单价（含退拒）

from dws_b2c_orders

group by user_id

),

t2 as (

select user_id,addr
from
(
select
user_id,
addr,
row_number() over(partition by user_id order by cnt desc) as rn
from
(
select
user_id,
concat_ws(',',nvl(area_name,''),nvl(address,''))  as addr, --地址
count(1) as cnt --次数
from dws_b2c_orders
group by user_id,concat_ws(',',nvl(area_name,''),nvl(address,''))
) o1

) o2
where rn=1
),

t3 as (

select user_id,pay_type
from
(
select user_id,pay_type,
row_number() over(partition by user_id order by cnt desc) as rn
from
(
select
user_id,pay_type,count(1) as cnt
from dws_b2c_orders
group by user_id,pay_type
) o1
) o2
where rn=1
),

t4 as (
select
user_id,
count(distinct session_id) as month1_cart_cnt, --添加购物车次数
sum(number) as month1_cart_goods_cnt ,-- 添加商品总件数
sum(if(submit_time is not null,number,0)) as month1_cart_submit_cnt , -- 提交的商品件数
-- 提交比率
sum(if(cancel_time is not null,number,0)) as month1_cart_cancel_cnt
-- 取消比率
from
(select * from ods_b2c_cart
where datediff('2019-06-16',add_time)<=30) cart
group by user_id
),

t5 as (

select user_id from dws_b2c_orders
union
select user_id from ods_b2c_cart

)

-- 总查询

insert into table ads_user_order_tag
select

 t5.user_id                                                                         ,
 t1.first_order_time                                                                ,
 t1.last_order_time                                                                 ,
 datediff('2019-06-16',t1.first_order_time)  as first_order_ago                     ,
 datediff('2019-06-16',t1.last_order_time)  as last_order_ago                       ,
 t1.month1_order_cnt                                                                ,
 t1.month1_order_amt                                                                ,
 t1.month2_order_cnt                                                                ,
 t1.month2_order_amt                                                                ,
 t1.month3_order_cnt                                                                ,
 t1.month3_order_amt                                                                ,
 t1.max_order_amt                                                                   ,
 t1.min_order_amt                                                                   ,
 t1.total_order_cnt                                                                 ,
 t1.total_order_amt                                                                 ,
 t1.total_coupon_amt                                                                ,
 t1.total_order_amt/t1.total_order_cnt  as user_avg_amt                             ,
 t1.month3_order_amt/t1.month2_order_cnt as month3_user_avg_amt                     ,
 t2.addr as common_address                                                          ,
 t3.pay_type as common_paytype                                                      ,
 t4.month1_cart_cnt                                                                 ,
 t4.month1_cart_goods_cnt                                                           ,
 t4.month1_cart_submit_cnt                                                          ,
 t4.month1_cart_submit_cnt/t4.month1_cart_goods_cnt   as  month1_cart_submit_rate   ,
 t4.month1_cart_cancel_cnt                                                          ,
 t4.month1_cart_cancel_cnt/t4.month1_cart_goods_cnt   as  month1_cart_cancel_rate   ,
 '2019-06-16' as dw_date
from t5
   join t1 on t5.user_id=t1.user_id
   join t2 on t5.user_id=t2.user_id
   join t3 on t5.user_id=t3.user_id
   join t4 on t5.user_id=t4.user_id
;



