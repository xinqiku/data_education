/*
    造 留存明细 测试数据
*/
drop table if exists yiee.demo_retention_dtl;
create table yiee.demo_retention_dtl(
dt string,
dnu_cnt int,
retention_days int,
retention_cnts int
)
row format delimited fields terminated by ',';

load data local inpath '/root/data/demo_retention_dtl.dat' into table demo_retention_dtl;
0601,100,1,80
0601,100,2,60
0601,100,3,50
0601,100,4,60
0601,100,5,40
0602,80,1,80
0602,80,2,50
0602,80,3,40
0602,80,4,30
0603,120,1,110
0603,120,2,80
0603,120,3,90
-- 计算：逻辑就是竖表转横表
insert into table yiee.ads_dnu_retention
select
dt,
dnu_cnt,
sum(if(retention_days=1,retention_cnts,0)) as ret_1_cnts,
sum(if(retention_days=2,retention_cnts,0)) as ret_2_cnts,
sum(if(retention_days=3,retention_cnts,0)) as ret_3_cnts,
sum(if(retention_days=4,retention_cnts,0)) as ret_4_cnts,
sum(if(retention_days=5,retention_cnts,0)) as ret_5_cnts,
sum(if(retention_days=6,retention_cnts,0)) as ret_6_cnts,
sum(if(retention_days=7,retention_cnts,0)) as ret_7_cnts,
sum(if(retention_days=14,retention_cnts,0)) as ret_14_cnts,
sum(if(retention_days=30,retention_cnts,0)) as ret_30_cnts
from yiee.demo_retention_dtl
group by dt,dnu_cnt


/*
    目标表：新用户留存报表：ads_dnu_retention
	日期    新增总数     1天后留存     2天后留存   3天后留存    4天后留存    5天后留存
*/
-- @dst 建表
create table yiee.ads_dnu_retention(
dt string,
dnu_cnt int,
ret_1_cnts int,
ret_2_cnts int,
ret_3_cnts int,
ret_4_cnts int,
ret_5_cnts int,
ret_6_cnts int,
ret_7_cnts int,
ret_14_cnts int,
ret_30_cnts int
)
stored as parquet
;

-- 计算：逻辑就是竖表转横表
insert into table yiee.ads_dnu_retention
select
dt,
dnu_cnt,
sum(if(retention_days=1,retention_cnts,0)) as ret_1_cnts,
sum(if(retention_days=2,retention_cnts,0)) as ret_2_cnts,
sum(if(retention_days=3,retention_cnts,0)) as ret_3_cnts,
sum(if(retention_days=4,retention_cnts,0)) as ret_4_cnts,
sum(if(retention_days=5,retention_cnts,0)) as ret_5_cnts,
sum(if(retention_days=6,retention_cnts,0)) as ret_6_cnts,
sum(if(retention_days=7,retention_cnts,0)) as ret_7_cnts,
sum(if(retention_days=14,retention_cnts,0)) as ret_14_cnts,
sum(if(retention_days=30,retention_cnts,0)) as ret_30_cnts
from yiee.demo_retention_dtl
group by dt,dnu_cnt
;


insert overwrite table yiee.demo_retention_dtl
select
first_login as dt,
count(1) as dnu_cnt,   -- 日新总数
datediff('2019-08-30',first_login) as retention_days,  --留存天数
count(if(last_login='2019-08-30',1,null)) as retention_cnts  --留存人数
from  demo_hisu_dtl
where datediff('2019-08-30',first_login) between 1 and 30
and last_login ='2019-08-30'
group by first_login
;













