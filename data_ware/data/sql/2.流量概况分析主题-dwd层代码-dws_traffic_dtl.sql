--创建数据明细层-流程明细表
drop table if exists yiee.dwd_traffic_dtl;
CREATE EXTERNAL TABLE
  IF NOT EXISTS yiee.dwd_traffic_dtl(
  uid String,
  osName String,   --操作系统名称
  osVer String,    --操作系统版本
  resolution String, --手机屏幕分辨率
  manufacture String, --手机生产厂商（品牌）
  appid String,     --所用的app的标识
  appVer String,    --所用的app的版本
  release_ch String, --所用的app的下载渠道（360应用市场，豌豆荚，小米应用....)
  promotion_ch String, --所用的app的推广渠道（头条，百度，抖音.....)
  carrier String,    --用户所用的移动运营商
  netType String,   --用户所用的网络类型（3g  4g  5g   wifi ）
  sessionId String,  --本条日志所在的会话id
  commit_time BIGINT, --本条日志发生的时间
  province String,
  city String,
  district String,
  url string
  )
  partitioned BY (dt String)
  stored AS parquet
--将贴源层的所有用户标识的字段提炼为一个字段
insert into table yiee.dwd_traffic_dtl partition(dt='2019-06-16')
select
  coalesce(
  if(trim(account) = '', null, account),
  if(trim(imei) = '', null, imei),
  if(trim(androidId) = '', null, androidId),
  if(trim(deviceId) = '', null, deviceId),
  if(trim(cookieid) = '', null, cookieid),
  if(trim(ip) = '', null, ip)
  ) as uid,
  osName ,   --操作系统名称
  osVer ,    --操作系统版本
  resolution , --手机屏幕分辨率
  manufacture , --手机生产厂商（品牌）
  appid ,     --所用的app的标识
  appVer ,    --所用的app的版本
  release_ch , --所用的app的下载渠道（360应用市场，豌豆荚，小米应用....)
  promotion_ch , --所用的app的推广渠道（头条，百度，抖音.....)
  carrier ,    --用户所用的移动运营商
  netType ,   --用户所用的网络类型（3g  4g  5g   wifi ）
  sessionId ,  --本条日志所在的会话id
  commit_time , --本条日志发生的时间
  province ,
  city ,
  district ,
  event['url'] as url

from

yiee.ods_eventlog
where dt='2019-06-16' and logType = 'pg_view'