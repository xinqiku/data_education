/*
   @主题：广告活动分析
   @dst： 广告事件分析的dwd层明细表: dwd_ade_dtl
   @src:  ods_eventlog
*/

-- @dst建表
drop table if exists dwd_ade_dtl;
create table dwd_ade_dtl(
uid string,
sessionid string,
event_type string,
commit_time bigint,
eventmap map<string,string>
)
partitioned by (dt string)
stored as parquet
;


-- etl计算
insert into table dwd_ade_dtl partition(dt='2019-06-16')
select
COALESCE(
 if(trim(account) ='',null,account),
 if(trim(imei) = '',null,imei),
 if(trim(androidid) = '',null,androidid),
 if(trim(deviceid) = '',null,deviceid),
 if(trim(cookieid) = '',null,cookieid)
) as uid,
sessionid,
eventtype,
commit_time,
event
from ods_eventlog
where dt='2019-06-16'
and eventtype='ad_show'
or
(event['ad_trace_id'] is not null and trim(event['ad_trace_id']) != '')
;








--  可以造一个测试数据来进行后续的计算


/*
  造数据
  
*/
vi ade.dat
u01,A,ad01,show
u01,A,ad01,show
u01,A,ad03,show
u01,A,ad03,show
u01,A,ad01,show
u01,A,ad02,show
u01,A,ad01,click
u01,A,ad02,click
u01,B,ad03,show
u01,B,ad03,show
u01,B,ad03,click
u01,B,ad03,click
u02,C,ad01,show
u02,C,ad01,show
u02,C,ad01,show
u02,C,ad01,show
u02,C,ad01,show
u02,C,ad01,show
u03,C,ad01,show
u03,C,ad01,show
u04,C,ad01,show
u04,C,ad01,show
u02,A,ad02,show
u02,A,ad02,click

drop table if exists demo_ade_dtl;
create table demo_ade_dtl(
uid string,
page string,
adid string,
eventtype string
)
row format delimited fields terminated by ',';


load data local inpath '/root/data/ade.dat' into table demo_ade_dtl;




