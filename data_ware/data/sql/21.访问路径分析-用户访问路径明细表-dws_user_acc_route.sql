/*
   访问路径分析：dws_user_acc_route
   @src  demo_dwd_event_dtl 事件记录明细表
   
   计算的逻辑细节：
		1. 考虑用户在同一个页面多次刷新，是否要重复计数的问题（此处公司想去重）
				可以将用户的访问记录，通过lead over错位，如果在同一行中，出现两个 B,A   B,A的组合，则可以去掉
		2. 一条记录在用户会话中是第几步访问，通过对上面整理后的数据打rownumber即可
		3. 详细看下图：
*/

-- 造demo测试数据
vi e.dat
u01,s01,pg_view,A,X,1
u01,s01,pg_view,B,A,2
u01,s01,pg_view,B,A,3
u01,s01,pg_view,B,A,4
u01,s01,pg_view,C,B,5
u01,s01,pg_view,D,C,6
u01,s01,pg_view,A,D,7
u01,s01,pg_view,B,A,8
u02,s21,pg_view,A,X,1
u02,s21,pg_view,B,A,2
u02,s21,pg_view,D,B,3
u02,s21,pg_view,B,D,4
u02,s21,pg_view,E,B,5
u02,s21,pg_view,F,E,6
u02,s21,pg_view,D,F,7
u02,s21,pg_view,B,D,8

-- 建demo测试表
drop table if exists demo_dwd_event_dtl;
create table demo_dwd_event_dtl(
uid string,
sessionid string,
event_type string,
url string,
reference string,
commit_time bigint
)
row format delimited fields terminated by ',';


load data local inpath '/root/data/demo_dwd_event_dtl.dat' into table demo_dwd_event_dtl;

-- 逻辑示意图
uid,sessionid,event_type,url,reference ,commit_time      lead() over()
u01, s01,   pg_view, A,    X           ,  time1          B,    A
u01, s01,   pg_view, B,    A           ,  time2          B,    A
u01, s01,   pg_view, B,    A           ,  time3          B,    A
u01, s01,   pg_view, B,    A           ,  time4          C,    B
u01, s01,   pg_view, C,    B           ,  time5          D,    C
u01, s01,   pg_view, D,    C           ,  time6          A,    D
u01, s01,   pg_view, A,    D           ,  time7          B,    A
u01, s01,   pg_view, B,    A           ,  time8          


--  @dst建表
create table ads_user_acc_route(
uid string,   --用户标识
sessionid string,  -- 会话标识
step int,   -- 访问步骤号
url string,  -- 访问的页面
ref string   -- 前一个页面（所来自的页面）
)
partitioned by (dt string)
stored as parquet
;
-- etl计算

-- 先过滤掉重复刷新的记录

with tmp as
(
select
uid,
sessionid,
event_type,
url,
reference,
commit_time,
concat_ws('-',url,reference) as tuple,
lead(concat_ws('-',url,reference),1) over(partition by uid,sessionid order by commit_time) as tuple2
from
demo_dwd_event_dtl
)

-- 得到结果
/*
+------+------------+-------------+------+------------+--------------+--------+---------+
| uid  | sessionid  | event_type  | url  | reference  | commit_time  | tuple  | tuple2  |
+------+------------+-------------+------+------------+--------------+--------+---------+
| u01  | s01        | pg_view     | A    | X          | 1            | A-X    | B-A     |
| u01  | s01        | pg_view     | B    | A          | 2            | B-A    | B-A     |
| u01  | s01        | pg_view     | B    | A          | 3            | B-A    | B-A     |
| u01  | s01        | pg_view     | B    | A          | 4            | B-A    | C-B     |
| u01  | s01        | pg_view     | C    | B          | 5            | C-B    | D-C     |
| u01  | s01        | pg_view     | D    | C          | 6            | D-C    | A-D     |
| u01  | s01        | pg_view     | A    | D          | 7            | A-D    | B-A     |
| u01  | s01        | pg_view     | B    | A          | 8            | B-A    | NULL    |
| u02  | s21        | pg_view     | A    | X          | 1            | A-X    | B-A     |
| u02  | s21        | pg_view     | B    | A          | 2            | B-A    | D-B     |
| u02  | s21        | pg_view     | D    | B          | 3            | D-B    | B-D     |
| u02  | s21        | pg_view     | B    | D          | 4            | B-D    | E-B     |
| u02  | s21        | pg_view     | E    | B          | 5            | E-B    | F-E     |
| u02  | s21        | pg_view     | F    | E          | 6            | F-E    | D-F     |
| u02  | s21        | pg_view     | D    | F          | 7            | D-F    | B-D     |
| u02  | s21        | pg_view     | B    | D          | 8            | B-D    | NULL    |
+------+------------+-------------+------+------------+--------------+--------+---------+
*/

-- 将上述中间结果中的tuple=tuple2的记录去除

select 
uid,
sessionid,
event_type,
url,
reference,
commit_time
from tmp
where tuple !<==> tuple2 or tuple2 is null

-- 并按同一个人的同一个会话的时间顺序 标记行号，对上步骤的sql略微改造一下：
select 
uid,
sessionid,
event_type,
url,
reference,
commit_time,
row_number() over(partition by uid,sessionid order by commit_time) as step
from tmp
where tuple != tuple2 or tuple2 is not null

-- 最后的完整语句

with tmp as
(
select
uid,
sessionid,
event_type,
url,
reference,
commit_time,
concat_ws('-',url,reference) as tuple,
lead(concat_ws('-',url,reference),1) over(partition by uid,sessionid order by commit_time) as tuple2
from 
demo_dwd_event_dtl
)

select 
uid,
sessionid,
event_type,
url,
reference,
commit_time,
row_number() over(partition by uid,sessionid order by commit_time) as step
from tmp
where tuple != tuple2
;













