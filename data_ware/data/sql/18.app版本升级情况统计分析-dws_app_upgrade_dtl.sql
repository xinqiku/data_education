/*
   app分析：app版本升级情况明细记录表
   @src  ： 流量明细表dwd_traffic_dtl
   @dst  ： app升级情况明细记录表 dws_app_upgrade_dtl
*/

-- 造一些测试数据填充到流量明细表demo_dwd_traffic_dtl

create table demo_dwd_traffic_dtl(
uid string,
day string,
appver string
) 
row format delimited fields terminated by ',';

load data local inpath '/root/ver.dat' into table demo_dwd_traffic_dtl;

vi ver.dat
a,2019-06-20,1.02
a,2019-06-20,1.02
a,2019-06-20,1.02
a,2019-06-20,2.0
a,2019-06-20,2.0
a,2019-06-20,2.5
a,2019-06-20,2.5
b,2019-06-20,1.2
b,2019-06-20,1.2
b,2019-06-20,2.5
c,2019-06-20,2.0
c,2019-06-20,2.0


-- 计算逻辑


with tmp as(
select
uid,day,appver
from  demo_dwd_traffic_dtl
group by uid,day,appver
)
/*
a,2019-06-20,1.02
a,2019-06-20,2.0
a,2019-06-20,2.5
b,2019-06-20,1.2
b,2019-06-20,2.5
c,2019-06-20,2.0
*/
-- 将上述数据按uid分窗口，按版本号排序
-- 然后将下一行的版本号，提到上一行

select * from 
(
select
uid,day,appver,
lead(appver,1) over(partition by uid order by appver) as next_ver
from  tmp
) o
where next_ver is not null
;

+------+-------------+---------+-----------+
| uid  |     day     | appver  | next_ver  |
+------+-------------+---------+-----------+
| a    | 2019-06-20  | 1.02    | 2.0       |
| a    | 2019-06-20  | 2.0     | 2.5       |
| b    | 2019-06-20  | 1.2     | 2.5       |
+------+-------------+---------+-----------+


















