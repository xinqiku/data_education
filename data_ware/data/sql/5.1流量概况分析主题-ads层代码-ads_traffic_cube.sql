/*
	@src：dws_traffic_agg_user
	@dst: ads_traffic_overview
*/

-- 获取各个维度的流量概况
drop table if exists yiee.ads_traffic_cube;
create table if not exists yiee.ads_traffic_cube(
dt string,
province string,
city string,
district string,
manufacture string,
osName string,
osVer string,
pv_cnts int,
uv_cnts int,
recome_user int
);

insert into table yiee.ads_traffic_cube
select
max(dt),
province,
city,
district,
manufacture,
osName,
osVer,
sum(pv_cnts) pv_cnts,
count(1) uv_cnts,
count(if(session_cnts>1, 1 , null))
from
yiee.dws_traffic_agg_uid
where dt = '2019-06-16'
group by province, city, district,
manufacture,
osName,
osVer
with cube