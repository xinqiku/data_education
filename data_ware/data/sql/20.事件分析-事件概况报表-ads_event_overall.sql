/*
	事件分析：事件概况报表
	@src ：dwd_event_dtl事件明细表
	@dst ：ads_event_overall
+---------------+---------+---------------+------------------------+---------------+----------------+----------------+-------------+
| t.event_type  |  t.uid  |  t.sessionid  |       t.location       | t.event_dest  | t.event_value  | t.commit_time  |    t.dt     |
+---------------+---------+---------------+------------------------+---------------+----------------+----------------+-------------+
| ad_show       | LXOGIR  | 3fkDqxogxMWE  | http://www.51doit.com  | 1             | NULL           | 1560672636603  | 2019-06-16  |
| ad_show       | LXOGIR  | 3fkDqxogxMWE  | http://www.51doit.com  | 5             | NULL           | 1560672636604  | 2019-06-16  |
| ad_show       | LXOGIR  | 3fkDqxogxMWE  | http://www.51doit.com  | 6             | NULL           | 1560672636605  | 2019-06-16  |
| ad_show       | 05289   | aNHKnDe44hS0  | http://www.51doit.com  | 1             | NULL           | 1560672636615  | 2019-06-16  |
| ad_show       | 05289   | aNHKnDe44hS0  | http://www.51doit.com  | 4             | NULL           | 1560672636616  | 2019-06-16  |
| ad_show       | 05289   | aNHKnDe44hS0  | http://www.51doit.com  | 5             | NULL           | 1560672636616  | 2019-06-16  |
| ad_show       | YFG6OV  | K3XBY77W6NkO  | http://www.51doit.com  | 1             | NULL           | 1560672636675  | 2019-06-16  |
| ad_show       | YFG6OV  | K3XBY77W6NkO  | http://www.51doit.com  | 2             | NULL           | 1560672636676  | 2019-06-16  |
| ad_show       | EK2MNA  | doJ9ymYGYXtU  | http://www.51doit.com  | 1             | NULL           | 1560672636679  | 2019-06-16  |
| ad_show       | EK2MNA  | doJ9ymYGYXtU  | http://www.51doit.com  | 4             | NULL           | 1560672636680  | 2019-06-16  |
| ad_show       | EK2MNA  | doJ9ymYGYXtU  | http://www.51doit.com  | 5             | NULL           | 1560672636680  | 2019-06-16  |
| ad_show       | 73129   | 7EpctWPksQIv  | http://www.51doit.com  | 1             | NULL           | 1560672636683  | 2019-06-16  |
| search        | 73129   | 7EpctWPksQIv  |                        | NULL          | NULL           | 1560672636683  | 2019-06-16  |
+---------------+---------+---------------+------------------------+---------------+----------------+----------------+-------------+
	
	
*/
-- 建表
create table ads_event_overall(
dt string,-- 统计日期
event_type string,  -- 事件名称
event_cishu int,   -- 事件发生的次数
event_renshu int  -- 事件发生的人数
)
stored as parquet
;


-- etl计算
-- 先按人聚合事件的次数
with tmp as (
select
'2019-06-16' as dt,
event_type,
uid,
count(1) as cishu
from dwd_event_dtl
where dt='2019-06-16'
group by event_type,uid
)

/*
ad_show  LXOGIR  3
ad_show  05289   3
ad_show  YFG6OV  2
ad_show  EK2MNA  3
ad_show  73129   1
*/
--insert into table ads_event_overall
select 
'2019-06-16' as dt,
event_type,
sum(cishu) as event_cishu,
count(1) as event_renshu
from tmp
group by event_type
;

