/*
    @主题：活跃用户新鲜度分析
	@src：历史用户记录表
	@dst：dws_active_fresh_dtl
	计算逻辑：
		先过滤出最后活跃日等于“计算日”的数据
			然后count ，就是“计算日”的总活跃数
			然后根据  最后活跃日-首登日 分组，求count，得出各种新鲜度下的人数
*/

-- @dst建表
drop table if exists yiee.dws_active_fresh_dtl;
create table yiee.dws_active_fresh_dtl(
dt string,
dau_amt int, -- 活跃总数
fresh int, -- 新鲜度(几天前新增的)
fresh_cnts int --新鲜的人数
)
stored as parquet
;

-- etl计算
set hive.strict.checks.cartesian.product = flase;
     set hive.mapred.mode = strict;
with tmp as (
select
uid,
first_login,
last_login,
datediff(last_login,first_login) as fresh
from yiee.demo_hisu_dtl
where last_login='2019-08-30' and datediff('2019-08-31',first_login) between 1 and 30
)

--insert into table yiee.dws_active_fresh_dtl
select
b.dt,    -- 计算日期
a.dau_amt,   -- 活跃总数
b.fresh,     -- 新鲜度
b.fresh_cnts  -- 新鲜人数
from
(
select count(1) as dau_amt from tmp
) a

join
(
select
'2019-08-30' as dt, -- 计算日期
fresh,  -- 新鲜度
count(1) as fresh_cnts -- 该新鲜度下的人数

from tmp
group by fresh
) b
; 








with tmp as (
select
last_login dt,
datediff(last_login,first_login) fresh,
count(1) fresh_cnts
from
demo_hisu_dtl
where last_login='2019-08-30' and datediff('${dt}',first_login) between 1 and 30
group by last_login, first_login

)

insert into table dws_active_fresh_dtl
select
b.dt, --计算日期
a.dau_amt, --活跃总数
b.fresh, --新鲜度
b.fresh_cnts--新鲜人数
from
(
select
count(1) dau_amt , last_login
from
demo_hisu_dtl
where last_login ='2019-08-30'
group by last_login
) a
join

(select dt,fresh, fresh_cnts from tmp) b
on a.last_login = b.dt


select
last_login,
concat(datediff(last_login,first_login),'天前新用户') as days,
count(1)  fresh_cnts,
sum(count(1)) over(partition by last_login )as dau_amt
from
demo_hisu_dtl
where last_login = '2019-08-30'
group by  last_login,first_login;










