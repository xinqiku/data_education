/*
    留存计算的第三种策略
*/

insert into yiee.dws_user_retention_dtl
select
first_login as dt,
count(1) as dnu_cnt,   -- 日新总数
datediff('2019-08-30',first_login) as retention_days,  --留存天数
count(if(last_login='2019-08-30',1,null)) as retention_cnts  --留存人数
from  yiee.demo_hisu_dtl
where datediff('2019-08-30',first_login) between 1 and 30
and last_login ='2019-08-30'
group by first_login, datediff('2019-08-30',first_login)
;