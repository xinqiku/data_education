-- 创建表
DROP TABLE

IF EXISTS yiee.ods_eventlog;

CREATE EXTERNAL TABLE
  IF NOT EXISTS yiee.ods_eventlog(

  cookieid String, --客户端id 用户的标识之一
  account String,  --账号 用户的标识之一
  imei String,     --苹果手机设备码id 用户的标识之一
  osName String,   --操作系统名称
  osVer String,    --操作系统版本
  resolution String, --手机屏幕分辨率
  androidId String,  --安卓系统的软id  用户的标识之一
  manufacture String, --手机生产厂商（品牌）
  deviceId String,    --设备id（用户标识之一）
  appid String,     --所用的app的标识
  appVer String,    --所用的app的版本
  release_ch String, --所用的app的下载渠道（360应用市场，豌豆荚，小米应用....)
  promotion_ch String, --所用的app的推广渠道（头条，百度，抖音.....)
  areacode BIGINT,   --业务系统中用于标识一个地理位置的码（废弃）
  longtitude DOUBLE, --本条日志发生的地点：经度
  latitude DOUBLE,   --本条日志发生的地点：维度
  carrier String,    --用户所用的移动运营商
  netType String,   --用户所用的网络类型（3g  4g  5g   wifi ）
  cid_sn String,   --废弃
  ip String,   -- ip 地址
  sessionId String,  --本条日志所在的会话id
  logType String,  --本条日志所代表的用户操作事件类别
  commit_time BIGINT, --本条日志发生的时间
  event Map < String, String >, --本条日志所代表的事件的详细信息（所发生的页面，前一个页面，所点击的商品skuid...)
  geoHash String,
  province String,
  city String,
  district String
  )
  partitioned BY (dt String)
  stored AS parquet

--加载数据
LOAD data inpath '/hivedata/eventlog/2019-06-16/' INTO TABLE ods_eventlog PARTITION (dt = '2019-06-16');
