/*
  活跃留存分析 
  @src 用户活跃记录拉链表
  @dst 活跃留存分析报表  ads_remain_active
*/
-- 日期  活跃总数  几日后   留存人数


-- @dst 活跃留存分析表建表
drop table if exists dws_remain_active;
create table dws_remain_active(
dt string comment '日期',
-- total_amt int comment '活跃总数', -- TODO，可以去join日活报表得出
remain_days int  comment '留存天数',
remain_cnts int   comment '留存人数'
)
stored as parquet;


-- etl计算
with tmp as (
select 
   explode(
     map(
       date_sub('2019-06-09',1),count(if(continue_start<=date_sub('2019-06-09',1) and continue_end='9999-12-31',1,null)),
       date_sub('2019-06-09',2),count(if(continue_start<=date_sub('2019-06-09',2) and continue_end='9999-12-31',1,null)),
       date_sub('2019-06-09',3),count(if(continue_start<=date_sub('2019-06-09',3) and continue_end='9999-12-31',1,null)),
       date_sub('2019-06-09',4),count(if(continue_start<=date_sub('2019-06-09',4) and continue_end='9999-12-31',1,null)),
       date_sub('2019-06-09',5),count(if(continue_start<=date_sub('2019-06-09',5) and continue_end='9999-12-31',1,null)),
       date_sub('2019-06-09',6),count(if(continue_start<=date_sub('2019-06-09',6) and continue_end='9999-12-31',1,null)),
       date_sub('2019-06-09',7),count(if(continue_start<=date_sub('2019-06-09',7) and continue_end='9999-12-31',1,null))
     ) 
   ) as (dt,cnts)
from  dws_user_continue_act

)
insert into table dws_remain_active
select
dt,
datediff('2019-06-09',dt) as remain_days,
cnts as remain_cnts
from tmp
;


-- 说明：
-- map构造完后的结果：
+------------------------------------------------------------------------------------------------------------+
|                         m                                                                                  |
+------------------------------------------------------------------------------------------------------------+
| {"2019-06-08":1,"2019-06-07":1,"2019-06-06":1,"2019-06-05":1,"2019-06-04":1,"2019-06-03":1,"2019-06-02":0} |
+------------------------------------------------------------------------------------------------------------+

-- 炸裂之后：
+-------------+--------+
|     key     | value  |
+-------------+--------+
| 2019-06-08  | 1      |
| 2019-06-07  | 1      |
| 2019-06-06  | 1      |
| 2019-06-05  | 1      |
| 2019-06-04  | 1      |
| 2019-06-03  | 1      |
| 2019-06-02  | 0      |
+-------------+--------+

-- 最后的结果：
+-------------+--------------+--------------+
|     dt      | remain_days  | remain_cnts  |
+-------------+--------------+--------------+
| 2019-06-08  | 1            | 1            |
| 2019-06-07  | 2            | 1            |
| 2019-06-06  | 3            | 1            |
| 2019-06-05  | 4            | 1            |
| 2019-06-04  | 5            | 1            |
| 2019-06-03  | 6            | 1            |
| 2019-06-02  | 7            | 0            |
+-------------+--------------+--------------+
