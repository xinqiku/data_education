/*
    @主题：新用户留存分析
	@源表：历史用户记录表 dwd_hisu_dtl
	@目标表：dws_user_retention_dtl
*/

-- 建表：
drop table if exists yiee.dws_user_retention_dtl;
create table yiee.dws_user_retention_dtl(
dt string, --日期
dnu_cnts int ,-- 新增总数
retention_days int ,-- 留存天数
retention_cnts int  -- 留存人数
)
stored as parquet
;

-- 计算

with tmp as (
select
uid,
first_login,
last_login,
datediff(last_login,first_login) as retention_days-- 留存天数
from yiee.dwd_hisu_dtl
where first_login  between  date_sub('2019-06-17',30) and date_sub('2019-06-17',1)
and last_login='2019-06-17'
)

--insert into table yiee.dws_user_retention_dtl
select
a.dt,
b.dnu_cnt,
a.retention_days,
a.retention_cnts
from
(
select
first_login as dt,
retention_days,
count(1) as retention_cnts
from tmp
group by first_login,retention_days
) a

join
(
select
dt,
dnu_cnt
from yiee.dws_dnu_cube
where dt between  date_sub('2019-06-17',30) and date_sub('2019-06-17',1)
and coalesce(province,city,district,release_ch,manufacture) is null
) b
on a.dt = b.dt
;


