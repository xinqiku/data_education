--
-- 订单与商品宽表
drop table if exists dws_orders_goods;
create table dws_orders_goods(
order_id            bigint    ,-- 订单ID
goods_id            bigint    ,-- 商品ID
size_id             bigint    ,-- 商品规格id
goods_price         double    ,-- 商品售价
goods_amount        bigint    ,-- 商品数量
cat1_id             bigint    ,-- 类目1ID
cat1_name           string    ,-- 类目1名称
cat2_id             bigint    ,-- 类目2ID
cat2_name          string    ,-- 类目2名称
cat3_id            bigint    ,-- 类目3id
cat3_name          string    ,-- 类目3名称
order_no           string    ,-- 订单流水号
order_date         string    ,-- 订单创建日期
user_id            bigint    ,-- 用户ID
user_name          string    ,-- 登录名
order_money        double    ,-- 订单金额  --扣除促销、减免之后的金额
order_type         string    ,-- 订单类型
order_status       string    ,-- 订单状态
pay_type           string    ,-- 支付类型
pay_status         string    ,-- 支付状态
order_source       string    ,-- 订单来源
dw_date            string     -- 数仓计算日期

)
stored as parquet
;
-- 宽表包含所有字段
insert into table dws_orders_goods
select
b.order_id         ,-- 订单ID
b.goods_id         ,-- 商品ID
b.size_id          ,-- 条码ID
b.goods_price      ,-- 商品价格
b.goods_amount     ,-- 数量
c.first_cat        ,-- 类目1ID
c.first_cat_name   ,-- 类目1名称
c.second_cat       ,-- 类目2ID
c.second_cat_name  ,-- 类目2名称
c.third_cat        ,-- 类目3ID
c.third_cat_name   ,-- 类目3名称
a.order_no         ,-- 订单号
a.order_date       ,-- 订单日期
a.user_id          ,-- 用户ID
a.user_name        ,-- 登录名
a.order_money      ,-- 订单金额  --扣除促销、减免之后的金额
a.order_type       ,-- 订单类型
a.order_status     ,-- 订单状态
a.pay_type         ,-- 支付类型
a.pay_status       ,-- 支付状态
a.order_source     ,-- 订单来源
'2019-06-16'
from ods_b2c_orders  a
  join ods_b2c_orders_goods b  on a.order_id = b.order_id
  join ods_b2c_goods c on b.goods_id= c.goods_id

--用户订单退拒商品指标计算
drop table if exists dm_user_goods_amt;
create table dm_user_goods_amt(
user_id                           bigint      ,-- 用户
p_sales_cnt                       bigint      ,-- 排除退拒商品销售数量
p_sales_amt                       double      ,-- 排除退拒商品销售金额
p_sales_cut_amt                   double      ,-- 排除退拒商品销售金额（扣促销减免）
h_sales_cnt                       bigint      ,-- 含退拒销售数量
h_sales_amt                       double      ,-- 含退拒销售金额
h_sales_cut_amt                   double      ,-- 含退拒销售金额（扣促销减免）
return_cnt                        bigint      ,-- 退货商品数量
return_amt                        double      ,-- 退货商品金额
reject_cnt                        bigint      ,-- 拒收商品数量
reject_amt                        double      ,-- 拒收商品金额
common_first_cat                  bigint      ,-- 最常购买商品一级类目名称
common_second_cat                 bigint      ,-- 最常购买商品二级类目名称
common_third_cat                  bigint      ,-- 最常购买商品三级类目名称
dw_date                           bigint
) partitioned by (dt string)
stored as parquet
;
with t1 as
(
select
user_id,
sum(if(order_status in ('退换', '拒收'), 0, goods_amount)) as p_sales_cnt,
sum(if(order_status in ('退换', '拒收'), 0, goods_amount*goods_price)) as p_sales_amt,

sum(goods_amount) as h_sales_cnt,
sum(goods_amount*goods_price) as h_sales_amt,

sum(if(order_status ='退换', 0, goods_amount)) as return_cnt,
sum(if(order_status ='退换', 0, goods_amount*goods_price)) as return_amt,

sum(if(order_status ='拒收', 0, goods_amount)) as reject_cnt,
sum(if(order_status ='拒收', 0, goods_amount*goods_price)) as reject_amt


from
dws_orders_goods
group by user_id),

t2 as (
select
user_id,
sum(if(order_status in ('退货','拒收'),0,order_money)) as p_sales_cut_amt, -- 排除退拒的总金额（扣除了优惠）
sum(order_money) as h_sales_cut_amt -- 含退拒的总金额（扣除了优惠）
from ods_b2c_orders
group by user_id
),
t3 as(

select
user_id,
cat1_name
from
(select
user_id,
cat1_name,
amt,
row_number() over(partition by user_id order by amt desc) rn
from(
select
user_id,
cat1_name,
sum(goods_amount) as amt
from
dws_orders_goods
group by user_id, cat1_name) o
)o1 where  rn = 1
)
,

t4 as (
select
user_id,
cat2_name
from
(
select
user_id,cat2_name,row_number() over(partition by user_id order by cat2_cnt desc) as rn
from
(
select
user_id,
cat2_name,
sum(goods_amount) as cat2_cnt
from dws_orders_goods
group by user_id,cat2_name
) o1
) o2
where rn=1
),

t5 as (
select
user_id,
cat3_name
from
(
select
user_id,cat3_name,row_number() over(partition by user_id order by cat3_cnt desc) as rn
from
(
select
user_id,
cat3_name,
sum(goods_amount) as cat3_cnt
from dws_orders_goods
group by user_id,cat3_name
) o1
) o2
where rn=1
)

select
t1.user_id              ,
t1.p_sales_cnt          ,
t1.p_sales_amt          ,
t2.p_sales_cut_amt      ,
t1.h_sales_cnt          ,
t1.h_sales_amt          ,
t2.h_sales_cut_amt      ,
t1.return_cnt           ,
t1.return_amt           ,
t1.reject_cnt           ,
t1.reject_amt           ,
t3.cat1_name            ,
t4.cat2_name            ,
t5.cat3_name            ,
'2019-06-16' first_cat

from t1
  join t2 on t1.user_id=t2.user_id
  join t3 on t1.user_id=t3.user_id
  join t4 on t1.user_id=t4.user_id
  join t5 on t1.user_id=t5.user_id
