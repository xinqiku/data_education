/*
	计算：用户活跃记录拉链表
	@src  上日拉链表  当日日活表
	@dst  当日拉链表
	本脚本使用的是测试日活表demo_dau_dtl
	在生产环境中，直接将表名替换成 dwd_dau_dtl 即可
*/



with dau as(
select dt,uid from demo_dau_dtl where dt='2019-06-09'
),
tmp as (
select
uid,first_login
from dws_user_continue_act
group by uid,first_login
having max(continue_end) != '9999-12-31'
)



insert overwrite table dws_user_continue_act

select
if(a.uid is not null,a.uid,b.uid) as uid,
if(a.uid is not null,a.first_login,b.dt) as first_login,
if(a.uid is not null,a.continue_start,b.dt) as continue_start,
case 
  when a.uid is null then '9999-12-31'
  when a.continue_end='9999-12-31' and b.uid is null then date_sub('2019-06-09',1)
  else a.continue_end
end as continue_end
from 
  dws_user_continue_act a
full join
  dau b  
on a.uid=b.uid 

union all

select  
tmp.uid,
tmp.first_login,
dau.dt as continue_start,
'9999-12-31' as continue_end

from tmp  join  dau on tmp.uid=dau.uid
;





