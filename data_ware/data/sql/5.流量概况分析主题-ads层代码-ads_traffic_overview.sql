/*
	@src：dws_traffic_agg_user
	@dst: ads_traffic_overview
*/

-- @dst表创建
drop table if exists yiee.ads_traffic_overview;

create table if not exists yiee.ads_traffic_overview(
dt string comment '日期',
pv_cnts int comment 'pv总数',
uv_cnts int comment 'uv总数',
session_cnts int,
avg_time_session double,
avg_session_user double,
avg_depth_user double,
avg_time_user double,
recome_user_ratio double
);

insert into table yiee.ads_traffic_overview
select
max(dt),
sum(pv_cnts) pv_cnts,
count(1) uv_cnts,
sum(session_cnts) session_cnts,
sum(time_amt)/sum(session_cnts) avg_time_session,
sum(session_cnts)/count(1)  avg_session_user,
sum(pv_cnts)/count(1) avg_depth_user,
sum(time_amt)/count(1) avg_time_user,
count(if(session_cnts>1, 1, null))/count(1) recome_user_ratio
from
dws_traffic_agg_uid
where dt = '2019-06-16';