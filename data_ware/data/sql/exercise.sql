create table t_version(
v_id string
)
row format delimited fields terminated by ',';

load data local inpath '/root/data/version.dat' into table t_version;

with tmp as
(select
v_id,
--,rank() over(order by v_id desc ) seq
(concat(lpad(split(v_id,'\\.')[0],2,'0') , lpad(split(v_id,'\\.')[1],2,'0') , lpad(if(split(v_id, '\\.')[2] is not null,split(v_id, '\\.')[2],-1),2,'0'))) str
from
t_version)

select v_id from
(
select
--rank() over(order by concat(first , middle, end1) desc )  as seq
v_id,

row_number() over(order by str desc) rn
from
tmp
)o  where  o.rn = 1;



with tmp as
(select
v_id,
--,rank() over(order by v_id desc ) seq
(concat(lpad(split(v_id,'\\.')[0],2,'0') , lpad(split(v_id,'\\.')[1],2,'0') , lpad(if(split(v_id, '\\.')[2] is not null,split(v_id, '\\.')[2],-1),2,'0'))) str
from
t_version)
select
v_id,
rank() over(order by str desc) - 1 seq
from
tmp