+--------+---------+----------+--------------+
| t.uid  | t.page  | t.ad_id  | t.eventtype  |
+--------+---------+----------+--------------+
| u01    | A       | ad01     | show         |
| u01    | A       | ad01     | show         |
| u01    | A       | ad03     | show         |
| u01    | A       | ad03     | show         |
| u01    | A       | ad01     | show         |
| u01    | A       | ad02     | show         |
| u01    | A       | ad01     | click        |
| u01    | A       | ad02     | click        |
| u01    | B       | ad03     | show         |
| u01    | B       | ad03     | show         |
| u01    | B       | ad03     | click        |
| u01    | B       | ad03     | click        |
| u02    | C       | ad01     | show         |
| u02    | C       | ad01     | show         |
| u02    | C       | ad01     | show         |
| u02    | C       | ad01     | show         |
| u02    | C       | ad01     | show         |
| u02    | C       | ad01     | show         |
| u03    | C       | ad01     | show         |
| u03    | C       | ad01     | show         |
| u04    | C       | ad01     | show         |
| u04    | C       | ad01     | show         |
| u02    | A       | ad02     | show         |
| u02    | A       | ad02     | click        |
+--------+---------+----------+--------------+
==>                                                         
with tmp as (

select 
uid,page,adid,eventtype,
count(1) as cnts
from demo_ade_dtl
group by uid,page,adid,eventtype

)

+------+-------+-------+------------+-------+
| uid  | page  | adid  | eventtype  | cnts  |
+------+-------+-------+------------+-------+
| u01  | A     | ad01  | click      | 1     |
| u01  | A     | ad01  | show       | 3     |
| u01  | A     | ad02  | click      | 1     |
| u01  | A     | ad02  | show       | 1     |
| u01  | A     | ad03  | show       | 2     |

| u01  | B     | ad03  | click      | 2     |
| u01  | B     | ad03  | show       | 2     |

| u02  | A     | ad02  | click      | 1     |
| u02  | A     | ad02  | show       | 1     |

| u02  | C     | ad01  | show       | 6     |

| u03  | C     | ad01  | show       | 2     |

| u04  | C     | ad01  | show       | 2     |
+------+-------+-------+------------+-------+


==>

select  
adid,page,eventtype,
sum(cnts) as cnts,
count(1) as users
from  tmp
group by  adid,page,eventtype
;
+-------+-------+------------+-------+--------+
| adid  | page  | eventtype  | cnts  | users  |
+-------+-------+------------+-------+--------+
| ad01  | A     | click      | 1     | 1      |
| ad01  | A     | show       | 3     | 1      |
| ad02  | A     | click      | 2     | 2      |
| ad02  | A     | show       | 2     | 2      |
| ad03  | A     | show       | 2     | 1      |
| ad03  | B     | click      | 2     | 1      |
| ad03  | B     | show       | 2     | 1      |
| ad01  | C     | show       | 10    | 3      |
+-------+-------+------------+-------+--------+


==>
select
adid,page,eventtype,cnts,users,
first_value(page) over(partition by adid,eventtype order by cnts desc)  as max_cnts_page
/*
| ad01  | C     | show       | 10    | 3      |   C
| ad01  | A     | show       | 3     | 1      |   C


| ad01  | A     | click      | 4     | 5      |   A
| ad01  | D     | click      | 4     | 2      |   A
*/

from   (

select  
adid,page,eventtype,
sum(cnts) as cnts,
count(1) as users
from  tmp
group by  adid,page,eventtype

)o1


+-------+-------+------------+-------+--------+----------------+                        +-------+-------+------------+-------+--------+
| adid  | page  | eventtype  | cnts  | users  | max_cnts_page  |                        | adid  | page  | eventtype  | cnts  | users  |
+-------+-------+------------+-------+--------+----------------+                        +-------+-------+------------+-------+--------+
| ad01  | A     | click      | 1     | 1      | A              |                        | ad01  | A     | click      | 1     | 1      |
| ad01  | C     | show       | 10    | 3      | C              |                        | ad01  | A     | show       | 3     | 1      |
| ad01  | A     | show       | 3     | 1      | C              |                        | ad02  | A     | click      | 2     | 2      |
| ad02  | A     | click      | 2     | 2      | A              |                        | ad02  | A     | show       | 2     | 2      |
| ad02  | A     | show       | 2     | 2      | A              |                        | ad03  | A     | show       | 2     | 1      |
| ad03  | B     | click      | 2     | 1      | B              |                        | ad03  | B     | click      | 2     | 1      |
| ad03  | B     | show       | 2     | 1      | B              |                        | ad03  | B     | show       | 2     | 1      |
| ad03  | A     | show       | 2     | 1      | B              |                        | ad01  | C     | show       | 10    | 3      |
+-------+-------+------------+-------+--------+----------------+                        +-------+-------+------------+-------+--------+

==> 再按 广告/类型 聚合  -- adid  type   cnts  users   曝光最大页   点击最大页

-- 建表dws_ade_overall 
create table dws_ade_overall(
dt string,
adid string,
eventtype string,
cnts int,
users int,
max_cnts_page string
)
stored as parquet;


-- etl 计算

with tmp as (

select 
uid,page,adid,eventtype,
count(1) as cnts
from demo_ade_dtl
group by uid,page,adid,eventtype

)

insert into table dws_ade_overall

select
'2019-06-16' as dt,
adid,eventtype,
sum(cnts) as cnts,
sum(users) as users,
max(max_cnts_page) as max_cnts_page

from  (
  select
  adid,page,eventtype,cnts,users,
  first_value(page) over(partition by adid,eventtype order by cnts desc)  as max_cnts_page
  from   (
  select  
  adid,page,eventtype,
  sum(cnts) as cnts,
  count(1) as users
  from  tmp
  group by  adid,page,eventtype
  
  )o1

)o2

group by adid,eventtype

+-------+------------+-------+--------+----------------+
| adid  | eventtype  | cnts  | users  | max_cnts_page  |
+-------+------------+-------+--------+----------------+
| ad01  | click      | 1     | 1      | A              |
| ad01  | show       | 13    | 4      | C              |
| ad02  | click      | 2     | 2      | A              |
| ad02  | show       | 2     | 2      | A              |
| ad03  | click      | 2     | 1      | B              |
| ad03  | show       | 4     | 2      | B              |
+-------+------------+-------+--------+----------------+


==>  可以把竖表变横表 得到最终报表 ads_ade_overall

-- 目标表建表：
create table ads_ade_overall(
dt string, -- 计算日期
adid string,
show_cnts int,
show_users int,
show_max_page string,
click_cnts int,
click_users int,
click_max_page string
)
stored as parquet;

-- etl
with a as (
select * from dws_ade_overall where eventtype='show'
),
b as (
select * from dws_ade_overall where eventtype='click'
)

insert into table ads_ade_overall

select 
'2019-06-16' as dt,
a.adid,
a.cnts as show_cnts,
a.users as show_users,
a.max_cnts_page as show_max_page,
b.cnts as click_cnts,
b.users as click_users,
b.max_cnts_page as click_max_page

from a full join b
on a.adid= b.adid
;




with tmp as
(
select
adid,
eventtype,
sum(cnts) cnts,
sum(users) users,
max(max_page) max_cnts_page
from

(select
adid, eventtype, sum(cnts) cnts, count(1) users, max(page) max_page
from
(select
adid, eventtype, cnts,
first_value(page) over(partition by eventtype, adid order by cnts desc) page
from
  (select
  uid, page, adid, eventtype,
  count(1) as cnts
  from
  demo_ade_dtl
  group by uid, page, adid, eventtype
  ) o1
)o2 group by adid, eventtype, page) o3
group by adid, eventtype
)


select
'2019-06-16' as dt,
a.adid,
a.cnts as show_cnts,
a.users as show_users,
a.max_cnts_page as show_max_page,
b.cnts as click_cnts,
b.users as click_users,
b.max_cnts_page as click_max_page
from
(select
*
from
tmp where eventtype = 'show') a
full join
(select
*
from
tmp where eventtype = 'click') b
on
a.adid = b.adid