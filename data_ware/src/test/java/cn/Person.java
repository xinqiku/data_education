package cn;

import lombok.Data;
import lombok.ToString;

/**
 * @program: data_education
 * @description:
 * @author: dyc
 * @create: 2019/09/10
 */
@ToString
public class Person {
    private String name;
    private int age;
    private String desc;
    public Person(){}
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
