import cn.Car;
import cn.Person;
import cn.Product;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * @program: data_education
 * @description: 规则引擎
 * @author: dyc
 * @create: 2019/09/10
 */
public class Drools7Test {

    static KieSession getSession() {
        KieServices ks = KieServices.Factory.get();
        KieContainer kc = ks.getKieClasspathContainer();
        return kc.newKieSession("simpleRuleKSession");
    }

    public static void main(String[] args) {
        KieSession ks = getSession();
        Person p1 = new Person("A", 68);
        Person p2 = new Person("B", 32);
        Person p3 = new Person("C", 18);
        Person p4 = new Person("D", 8);

        System.out.println("before"+p1);
        System.out.println("before"+p2);
        System.out.println("before"+p3);
        System.out.println("before"+p4);

        ks.insert(p1);
        ks.insert(p2);
        ks.insert(p3);
        ks.insert(p4);

        int count = ks.fireAllRules();
        System.out.println(count + "条规则");
        System.out.println(p1);
        ks.dispose();
    }

    public void testDrool7Api(){
        KieServices kieServices = KieServices.Factory.get(); // 通过这个静态方法去获取一个实例
        KieContainer kieContainer = kieServices.getKieClasspathContainer();// 默认去读取配置文件
        KieSession kieSession = kieContainer.newKieSession("all-rules");// 根据这个名词去获取kieSession

        Person p1 = new Person();
        p1.setAge(30);
        Car c1 = new Car();
        c1.setPerson(p1);

        Person p2 = new Person();
        p1.setAge(70);
        Car c2 = new Car();
        c2.setPerson(p2);

        kieSession.insert(c1); // 将c1实例放入到session中,
        kieSession.insert(c2); //

        int count = kieSession.fireAllRules();// 开始执行规则,并获取执行了多少条规则
        kieSession.dispose();// 关闭session
        System.out.println("Fire " + count + " rules(s)!");
        System.out.println("The discount of c1 is " + c1.getDiscount() + "%");
        System.out.println("The discount of c2 is " + c2.getDiscount() + "%");
    }

    @org.junit.Test
    public void testRules() {
        // 构建KieServices
        KieServices ks = KieServices.Factory.get();
        KieContainer kieContainer = ks.getKieClasspathContainer();
        // 获取kmodule.xml中配置中名称为ksession-rule的session，默认为有状态的。
        KieSession kSession = kieContainer.newKieSession("ksession-rule");

        Product product = new Product();
        product.setType(Product.GOLD);
        System.out.println("商品" +product.getType() + "的商品折扣为" + product.getDiscount() + "%。");
        kSession.insert(product);
        int count = kSession.fireAllRules();
        System.out.println("命中了" + count + "条规则！");
        System.out.println("商品" +product.getType() + "的商品折扣为" + product.getDiscount() + "%。");

    }

}
