package cn.dyc.dsl

import cn.dyc.util.SparkUtil
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{DataType, DataTypes, StructType}
import org.apache.spark.sql.{Dataset, Row, SparkSession}

object DslDemo {
  def main(args: Array[String]): Unit = {
    val session: SparkSession = SparkUtil.getSparkSession()
    val df = session.read.csv("data_ware/data/text/stu.txt").toDF("id","name","age")

    val schema: StructType = df.schema
    val rdd: RDD[Row] = df.rdd.map(row => {
      row.getAs[String]("name")
      val x = "aa"
      val seq: Seq[Any] = row.toSeq
      val newseq: Seq[Any] = seq.:+(x)
      Row(newseq: _*)
    })
    session.createDataFrame(rdd, schema.add("x",DataTypes.StringType))
  }
}
