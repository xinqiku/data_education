package cn.dyc.crawler

import cn.dyc.util.SparkUtil
import org.apache.spark.sql.{DataFrame, SparkSession}

object ShowParquet {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkUtil.getSparkSession(this.getClass.getName)
    val frame: DataFrame = spark.read.parquet("data_ware/data/eventlog_out/2019-06-16/")

    frame.show(10, false)
    spark.close()
  }
}
