package cn.dyc.area

import java.util.Properties

import ch.hsr.geohash.GeoHash
import cn.dyc.util.SparkUtil

object AreaDictFromFile {
  def main(args: Array[String]): Unit = {

    val session = SparkUtil.getSparkSession()
    import session.implicits._
    val properties = new Properties()
    properties.setProperty("user", "root")
    properties.setProperty("password", "123456")
    val dataFrame = session.read.jdbc("jdbc:mysql://localhost:3306/test", "t_area_dict",properties)
    val frame = dataFrame.map(row => {
      val longitude = row.getAs[String]("baidu_longitude")
      val latitude = row.getAs[String]("baidu_latitude")
      val district = row.getAs[String]("district")
      val city = row.getAs[String]("city")
      val province = row.getAs[String]("province")
      val base32 = GeoHash.withCharacterPrecision(latitude.toDouble, longitude.toDouble, 5).toBase32

      (base32, province, city, district)
    }).toDF("geo", "province", "city", "district").distinct()

    frame.coalesce(1).write.jdbc("jdbc:mysql://localhost:3306/test", "t_area_code",properties)
    frame.coalesce(1).write.parquet("data_ware/data/dict/out_area_dict")

    /**
      *   df 结构化数据
      */
    val geofun = (longitude:String, latitude:String) => {
      GeoHash.withCharacterPrecision(latitude.toDouble, longitude.toDouble, 5).toBase32
    }
    session.udf.register("geofun", geofun)

    dataFrame.createTempView("area")

    val df = session.sql(
      """
        |
        |select geofun(baidu_longitude, baidu_latitude) as geo ,
        |province,
        |city,
        |district
        |from
        |area
        |
      """.stripMargin).distinct()

    //df.show(10, false)

    /**
      * dsl 风格
      */

    import org.apache.spark.sql.functions._

    val geofun2 = udf(geofun)

    val res = dataFrame.select(geofun2('baidu_longitude, 'baidu_latitude) as "geo", 'province, 'city, 'district).distinct()
//    res.coalesce(1).write.parquet("data_ware/data/dict/out_area_dict")
//    res.show(10, false)
  }
}
