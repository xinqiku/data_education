package cn.dyc.area

import java.util.Properties

import org.apache.spark.sql.SparkSession

object AreaLocation {
  def main(args: Array[String]): Unit = {
    val session = SparkSession.builder().appName(this.getClass.getName).master("local[*]"). getOrCreate()
    val properties = new Properties()
    properties.setProperty("user", "root")
    properties.setProperty("password", "123456")
    val dataFrame = session.read.jdbc("jdbc:mysql://localhost:3306/test", "t_md_areas",properties)

    dataFrame.coalesce(1).write.parquet("data_ware/data/dict/out_area")
    session.close()
  }
}
