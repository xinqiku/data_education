package cn.dyc.area

import java.util.Properties

import org.apache.spark.sql.SparkSession

object AreaDict {
  def main(args: Array[String]): Unit = {
    val session = SparkSession.builder().appName(this.getClass.getName).master("local[*]"). getOrCreate()
    val properties = new Properties()
    properties.setProperty("user", "root")
    properties.setProperty("password", "123456")
    val dataFrame = session.read.jdbc("jdbc:mysql://localhost:3306/test", "t_md_areas",properties)

    dataFrame.show(10)
    dataFrame.createTempView("area")

    val df = session.sql(
      """
        |
        |SELECT
        |	t1.baidu_longitude,
        |	t1.baidu_latitude,
        |	t4.address AS province,
        |	t3.address AS city,
        |	t2.address AS district
        |FROM
        |	(
        |		SELECT
        |			t.*
        |		FROM
        |			area t
        |		WHERE
        |			t.area_type = 4
        |		AND t.baidu_longitude IS NOT NULL
        |		AND t.baidu_latitude IS NOT NULL
        |	) t1
        |JOIN area t2 ON t1.pid = t2.id
        |LEFT JOIN area t3 ON t2.pid = t3.id
        |LEFT JOIN area t4 ON t3.pid = t4.id
        |
        |
      """.stripMargin)

    df.coalesce(1).write.parquet("data_ware/data/dict/out_area_dict")

    session.close()
  }
}
