package cn.dyc.route

import java.util.Properties

import cn.dyc.util.{SparkUtil, TransactionRouteMatch}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.{DataFrame, Dataset, RelationalGroupedDataset}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
*@Description: 不同业务流程的完成步骤的人数 | 用户id | 业务id |完成步骤 |
*@Author: dyc
*@date: 2019/9/7
*/
object FunnelAnalysis {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)

    val spark = SparkUtil.getSparkSession()

    // 加载业务路径定义 元数据
    val props = new Properties()
    props.setProperty("user","root")
    props.setProperty("password","123456")

    val routeDefine = spark.read.jdbc("jdbc:mysql://localhost:3306/test","transaction_route",props)
    //获取用户定义的业务流程步骤
    val routeMap: collection.Map[String, List[(Int, String)]] = routeDefine.rdd.map(row => {
      val t_id = row.getAs[String]("tid")
      val t_event = row.getAs[String]("event_id")
      val t_step = row.getAs[Int]("step")

      (t_id, (t_step, t_event))
    }).groupByKey().mapValues(it => {
      val tuples: List[(Int, String)] = it.toList.sortBy(_._1)
      tuples
    }).collectAsMap()
    val bc: Broadcast[collection.Map[String, List[(Int, String)]]] = spark.sparkContext.broadcast(routeMap)

    // 加载用户访问路径数据
    val userRoute = spark.read.option("header","true").csv("data_ware/data/dws_user_acc_route/part-00000.csv")
    import spark.implicits._
    //val ds: RelationalGroupedDataset = userRoute.selectExpr("uid","sid", "collect_set(concat_ws('-',step,url,ref)").groupBy('uid,'sid)
    userRoute.createTempView("t")
    val df = spark.sql(
      """
        |with tmp as (select * from t order by step)
        |select uid,sid,
        |collect_list(url) as url_order
        |from
        |tmp group by uid, sid
        |
      """.stripMargin)
    //获取每个用户一次会话操作的步骤
    val usStep = df.map(row => {
      val uid: String = row.getAs[String]("uid")
      val sid: String = row.getAs[String]("sid")

      val strings: Seq[String] = row.getAs[Seq[String]]("url_order")
      ((uid,sid), strings.mkString)
    })

//    usStep.show(10, false)
    //获取每个用户一次会话操作的步骤 进行业务流程匹配
    val ustidstep = usStep.flatMap(t => {
      val uid = t._1._1
      val userlist = t._2.toList.map(_.toString)
      val routeMap: collection.Map[String, List[(Int, String)]] = bc.value
      val resList = new ListBuffer[(String, String, Int)]
      //对每个业务流程匹配 用户操作步骤
      for ((k, v) <- routeMap) {
        val routeList: List[String] = v.map(t => t._2)
        val steps: List[Int] = TransactionRouteMatch.routeMatch(userlist, routeList)
        val tuples: List[(String, String, Int)] = steps.map(i => {
          (uid, k, i)
        })
        resList ++= tuples
      }
      resList
    }).toDF("uid", "sid", "step")
    ustidstep.show(10,false)

    spark.close()

  }

}
