package cn.dyc.eventlog

import java.util.Properties

import cn.dyc.util.SparkUtil
import cn.dyc.util.eventlog.{EventJson2Bean, UserEventLog}
import com.alibaba.fastjson.{JSON, JSONException, JSONObject}
import org.apache.commons.lang3.StringUtils
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.{DataFrame, Dataset, Row}

import scala.collection.mutable


object EventLogETL {
  def main(args: Array[String]): Unit = {
    val cnfMap = new mutable.HashMap[String,String]
    if (args.size != 3) {
      println(
        """
          |
          |Usage:
          |args(0):地理位置字典路径
          |args(1):待处理的日志文件路径
          |args(2):预处理完成的日志数据输出路径
          |
        """.stripMargin)
      sys.exit(1)
    }

    cnfMap("spark.serializer") = classOf[KryoSerializer].getName

    val spark = SparkUtil.getSparkSession(this.getClass.getName, "yarn",cnfMap.toMap)

    import spark.implicits._
//1566969866926  1566969869388
//    val props = new Properties()
//    props.setProperty("user", "root")
//    props.setProperty("password", "123456")
//    val df: DataFrame = spark.read.jdbc("jdbc:mysql://192.168.225.99:3306/test", "t_area_code", props)
//    val geoaddr: Dataset[(String, (String, String, String))] = df.map(row => {
//      val geo: String = row.getAs[String]("geo")
//      val province: String = row.getAs[String]("province")
//      val city: String = row.getAs[String]("city")
//      val district: String = row.getAs[String]("district")
//      (geo, (province, city, district))
//    })
//    val geoMap: collection.Map[String, (String, String, String)] = geoaddr.rdd.collectAsMap()
// 或者从“文件”中加载地理位置字典
    val dictDF = spark.read.parquet(args(0))

    // 通过广播变量，发到日志处理的 task 端，关联时不需要shuffle（map端join）
    val areaMap = dictDF.map({
      case Row(geo: String, province: String, city: String, district: String)
      => {
        (geo, (province, city, district))
      }
    }).rdd.collectAsMap()
    val broadcast: Broadcast[collection.Map[String, (String, String, String)]] = spark.sparkContext.broadcast(areaMap)


    val ds: Dataset[String] = spark.read.textFile(args(1))
    val userelDs: Dataset[UserEventLog] = ds.map(it => {
      //val map: collection.Map[String, (String, String, String)] = broadcast.value
//      broadcast.value.toMap
//      it.map(line => {
        val strings: Array[String] = it.split(" --> ")

        if (strings.size < 2) {
          null
        } else {
          val userjson = strings(1)
          EventJson2Bean.genEventBean(userjson, broadcast.value)
        }

//      })
    })

    val filtered: Dataset[UserEventLog] = userelDs.filter(bean => bean != null)
//    val x = filtered.coalesce(1)
//    x.cache()
//    x.count()


    //获取没有省市区的数据
 /*   val nogps = filtered.filter(bean => StringUtils.isBlank(bean.province))
      .map(bean => (bean.longtitude, bean.latitude)).toDF("lng", "lat")


    nogps.coalesce(1).write.parquet("data_ware/data/toparse_gps/2019-06-16")*/
//    println(System.currentTimeMillis())
    filtered.coalesce(1).write.parquet(args(2))
//    println(System.currentTimeMillis())
    //filtered.write.parquet("hdfs://linux1:9000/eventlog/2019-06-16/")

    spark.stop()
  }
}
