package cn.dyc.eventlog


import cn.dyc.util.SparkUtil
import cn.dyc.util.eventlog.UserEventLog
import com.alibaba.fastjson.{JSON, JSONObject}
import org.apache.spark.sql.Dataset

object EventLogETLA {
  def main(args: Array[String]): Unit = {
    val spark = SparkUtil.getSparkSession(this.getClass.getName)

    val ds: Dataset[String] = spark.read.textFile("E:\\data\\eventlog\\2019-06-16\\16.doit.mall.access.log")
    import spark.implicits._
    val userelDs: Dataset[UserEventLog] = ds.map(line => {
      val strings: Array[String] = line.split("-->")
      val userjson = strings(1)
      var userEventLog: UserEventLog = null

      try {
        val userlogObject: JSONObject = JSON.parseObject(userjson)
        userEventLog = UserEventLog()
        val uob: JSONObject = userlogObject.getJSONObject("u")
        if(uob != null) {

          userEventLog.cookieid = uob.getString("cookieid")
          userEventLog.account = uob.getString("account")
          val phoneob: JSONObject = uob.getJSONObject("phone")
          if (phoneob != null) {
            userEventLog.imei = uob.getJSONObject("phone").getString("imei")
            userEventLog.osName = uob.getJSONObject("phone").getString("osName")
            userEventLog.osVer = uob.getJSONObject("phone").getString("osVer")
            userEventLog.resolution = uob.getJSONObject("phone").getString("resolution")
            userEventLog.androidId = uob.getJSONObject("phone").getString("androidId")
            userEventLog.manufacture = uob.getJSONObject("phone").getString("manufacture")
            userEventLog.deviceId = uob.getJSONObject("phone").getString("deviceId")
          }
          val app: JSONObject = uob.getJSONObject("app")
          if (app != null) {

            userEventLog.appid = uob.getJSONObject("app").getString("appid")
            userEventLog.appVer = uob.getJSONObject("app").getString("appVer")
            userEventLog.release_ch = uob.getJSONObject("app").getString("release_ch")
            userEventLog.promotion_ch = uob.getJSONObject("app").getString("promotion_ch")
          }
          val loc: JSONObject = uob.getJSONObject("loc")
          if (loc != null) {

            userEventLog.areacode = uob.getJSONObject("loc").getLong("areacode")
            userEventLog.longtitude = uob.getJSONObject("loc").getDouble("longtitude")
            userEventLog.latitude = uob.getJSONObject("loc").getDouble("latitude")
            userEventLog.carrier = uob.getJSONObject("loc").getString("carrier")
            userEventLog.netType = uob.getJSONObject("loc").getString("netType")
            userEventLog.ip = uob.getJSONObject("loc").getString("ip")
          }
          userEventLog.sessionId = uob.getString("sessionId")
        }
        userEventLog.commit_time = userlogObject.getLong("commit_time")
        import scala.collection.JavaConversions._
        userEventLog.event = userlogObject.getJSONObject("event").getInnerMap.map(t=> (t._1,t._2.toString)).toMap

        //过滤无效数据
        if (userEventLog.event == null ||
          ((userEventLog.account == null || userEventLog.account == "") &&
            (userEventLog.cookieid == null || userEventLog.cookieid == "") &&
            (userEventLog.sessionId == null || userEventLog.sessionId == "") &&
            (userEventLog.imei == null || userEventLog.imei == "") &&
            (userEventLog.deviceId == null || userEventLog.deviceId == "") &&
            (userEventLog.androidId == null || userEventLog.androidId == "") &&
            (userEventLog.ip == null || userEventLog.ip == ""))
        ) {
          userEventLog = null
        }

      } catch {
        case e: Throwable => null
      }

      userEventLog
    })

    val filtered: Dataset[UserEventLog] = userelDs.filter(bean => bean != null)
        val stringres: Dataset[String] = filtered.map(bean => {

          bean.toString
        })


    stringres.coalesce(1).write.csv("data_ware/data/csv/out_log")
    spark.stop()
  }
}
