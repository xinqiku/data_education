import cn.dyc.util.SparkUtil
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, SparkSession}

object ShowPar {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    val spark: SparkSession = SparkUtil.getSparkSession(this.getClass.getName)
    val frame: DataFrame = spark.read.parquet("user_profile/data/output/dsplog/day01")

    frame.show(10, false)
    spark.close()
  }
}
