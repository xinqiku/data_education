package cn.dyc.crawler

import cn.dyc.util.SparkUtil
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.types.{DataTypes, StructType}

/**
  * @program: data_education
  * @description: ${description}
  * @author: dyc
  * @create: 2019/09/16
  */
object TLogProcess {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    val spark = SparkUtil.getSparkSession(this.getClass.getName)
    //加载T日idmapping
    import spark.implicits._
    val idmpdf = spark.read.parquet("user_profile/demodata/graphx/output")
    val idmapping = idmpdf.map(row => {
      val gid = row.getAs[Long]("gid")
      val id = row.getAs[String]("id")
      (id, gid)
    }).rdd.collectAsMap()
    val idmpbc = spark.sparkContext.broadcast(idmapping)
    //加载日志
    val schema = new StructType()
      .add("phone", DataTypes.StringType)
      .add("name", DataTypes.StringType)
      .add("wx", DataTypes.StringType)
      .add("income", DataTypes.IntegerType)
    val logdatadf = spark.read.schema(schema).option("header", true).csv("user_profile/demodata/graphx/input")
    //根据日志id 和idmapping 获取日志对应gid
    val gidlog = logdatadf.map(row => {

      val idmp = idmpbc.value
      val phone = row.getAs[String]("phone")
      val name = row.getAs[String]("name")

      val wx = row.getAs[String]("wx")

      val income = row.getAs[Int]("income")
      val id = Array(phone, name, wx).filter(StringUtils.isNotBlank(_))(0)
      val gidOption = idmp.get(id)
      var gid = "-1"
      if (gidOption.isDefined) gid = gidOption.get + ""

      (gid, phone, name, wx, income)
    })
    //输出
    gidlog.toDF("gid","phone","name","wx","income").write.option("header", true).csv("user_profile/demodata/graphx/gidlog_out")

    spark.close()

  }
}
