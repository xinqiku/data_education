package cn.dyc.crawler

import cn.dyc.util.SparkUtil
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * @program: data_education
  * @description: ${description}
  * @author: dyc
  * @create: 2019/09/12
  */
object GraphIdGid {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    //获取所有的行处理
    val spark: SparkSession = SparkUtil.getSparkSession(this.getClass.getName)
    import spark.implicits._
    val df: DataFrame = spark.read.option("header",true).csv("user_profile/demodata/graphx/input/demo1.dat")
    df.cache()
    //组合顶点
    val vertices: RDD[(Long, String)] = df.rdd.flatMap(row => {
      val phone: String = row.getAs[String]("phone")
      val name: String = row.getAs[String]("name")
      val wx: String = row.getAs[String]("wx")
      Array(phone, name, wx).filter(StringUtils.isNotBlank(_)).map(e => (e.hashCode.toLong, e))

    })
    //组合边
    val edges: RDD[Edge[String]] = df.rdd.flatMap(row => {
      val phone: String = row.getAs[String]("phone")
      val name: String = row.getAs[String]("name")
      val wx: String = row.getAs[String]("wx")
      val ids: Array[String] = Array(phone, name, wx).filter(StringUtils.isNotBlank(_))
      for (i <- 1 until ids.size) yield Edge(ids(0).hashCode.toLong, ids(i).hashCode.toLong, "")
    })
    edges.take(10).foreach(println)
    //构建图
    val graph = Graph(vertices, edges)
    //获取子图(顶点id,连通顶点id的最小值, 原来的边)
    val childrenGraph: Graph[VertexId, String] = graph.connectedComponents()

    val childrenedges: EdgeRDD[String] = childrenGraph.edges
    childrenedges.take(10).foreach(println)
    //获取(顶点, 连通顶点id的最小值)
    val connected: VertexRDD[VertexId] = childrenGraph.vertices
    connected.take(10).foreach(println)

    val vJoin: RDD[(VertexId, (String, VertexId))] = vertices.join(connected)

    val idgidRdd: RDD[(String, VertexId)] = vJoin.map(t => {
      (t._2._1, t._2._2)
    })
    idgidRdd.toDF("id","gid").write.parquet("user_profile/demodata/graphx/output")

    spark.close()
  }
}
