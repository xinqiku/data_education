package cn.dyc.profile.preprocess

import ch.hsr.geohash.GeoHash
import cn.dyc.util.SparkUtil
import org.apache.commons.lang3.StringUtils
import cn.dyc.util.DictsLoader._
import org.apache.log4j.{Level, Logger}
/**
*@Description:
*@Author: dyc
*@date: 2019/9/17
*/
object DspLogPre {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    val spark = SparkUtil.getSparkSession(this.getClass.getSimpleName)
    import spark.implicits._
    // 加载原始数据
    val dsplog = spark.read.textFile("user_profile/data/dsplog/day01")


    // 加载app信息字典
    val appMap = loadAppDict(spark,"user_profile/data/appdict")
    val bcApp = spark.sparkContext.broadcast(appMap)


    // 加载地域信息字典
    val areaMap =  loadAreaDict(spark,"user_profile/data/areadict")
    val bcArea = spark.sparkContext.broadcast(areaMap)

    // 加载idmp映射字典
    val idmpMap = loadIdmpDict(spark,"user_profile/demodata/idmp/output/day02")
    val bcIdmap = spark.sparkContext.broadcast(idmpMap)

    // 切分字段
    dsplog
      .rdd
      .map(line => {
        line.split(",", -1)
      })
      .filter(arr => {
        // 过滤
        val flag1 = arr.size == 85

        val ids = for (i <- 46 to 50) yield arr(i)
        val idsStr = ids.mkString("").replaceAll("null", "").trim

        val flag2 = !idsStr.equals("")

        flag1 && flag2
      })
      // 数据集成，封装bean
      .map(arr => DspLogBean.genDspLogBean(arr))
      .map(bean => {

        // 从广播变量中取出3个字典
        val appDict = bcApp.value
        val areaDict = bcArea.value
        val idmpDict = bcIdmap.value

        // 定义好要追加集成的字段
        var province = bean.provincename
        var city = bean.cityname
        var district = bean.district
        var appname = bean.appname
        var appdesc = bean.appDesc
        var gid = "-1"


        // 取出gps坐标，并集成省市区信息  data_ware/data/dict/out_area_dict
        val lng: Double = bean.lng
        val lat: Double = bean.lat
        if (lng > 73 && lng < 140 && lat > 3 && lat < 54) {
          val geo: String = GeoHash.withCharacterPrecision(lat, lng, 5).toBase32
          val areaOption = areaDict.get(geo)
          if (areaOption.isDefined) {
            val area = areaOption.get
            province = area._1
            city = area._2
            district = area._3
          }
        }

        // 取出appid，并集成app信息  user_profile/data/appdict/app_dict.txt
        val appOption = appDict.get(bean.appid)
        if (appOption.isDefined) {
          val appInfo = appOption.get
          appname = appInfo._1
          appdesc = appInfo._2
        }


        // 取出5个id标识，并集成gid   user_profile/data/idmpdict/day01
        val imei = bean.imei
        val idfa = bean.idfa
        val udid = bean.openudid
        val andid = bean.androidid
        val mac = bean.mac
        val id = Array(imei, idfa, udid, andid, mac).filter(StringUtils.isNotBlank(_)).head

        gid = idmpDict.getOrElse(id.hashCode.toLong, -1L).toString

        // 返回追加完字段的bean
        bean.provincename = province
        bean.cityname = city
        bean.district = district
        bean.appname = appname
        bean.appDesc = appdesc
        bean.gid = gid

        bean
      })
      // 保存结果
      .toDF()
      .coalesce(1)
      .write
      .parquet("user_profile/data/output/dsplog/day01")

    spark.close()

  }

}
