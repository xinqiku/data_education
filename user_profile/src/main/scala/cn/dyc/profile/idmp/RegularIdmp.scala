package cn.dyc.profile.idmp

import cn.dyc.util.{IdsExtractor, SparkUtil}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph, VertexId, VertexRDD}
import org.apache.spark.sql.Row

/**
  * @program: data_education
  * @author: dyc
  * @create: 2019/09/17
  * @description:
  */
object RegularIdmp {
  def main(args: Array[String]): Unit = {
    //获取T+1数据ids
    Logger.getLogger("org").setLevel(Level.WARN)
    val spark = SparkUtil.getSparkSession()
    import spark.implicits._
//    val cmccPath = "user_profile/demodata/idmp/input/day02/cmcclog"
//    val dspPath = "user_profile/demodata/idmp/input/day02/dsplog"
//    val eventPath = "user_profile/demodata/idmp/input/day02/eventlog"
//    val cmccrdd = IdsExtractor.processDemoIds(spark, cmccPath)
//    val dsprdd = IdsExtractor.processDemoIds(spark, dspPath)
//    val eventrdd = IdsExtractor.processDemoIds(spark, eventPath)
    val cmccLogPath = "user_profile/data/cmcclog/day01"
    val dspLogPath = "user_profile/data/dsplog/day01"
    val eventLogPath = "user_profile/data/eventlog/day01"

    val cmccrdd = IdsExtractor.extractCmccLogIds(spark,cmccLogPath)
    val dsprdd = IdsExtractor.extractDspLogIds(spark,dspLogPath)
    val eventrdd = IdsExtractor.extractEventLogIds(spark,eventLogPath)
    val ids = cmccrdd.union(dsprdd).union(eventrdd)
    //获取顶点
    val vertices = ids.flatMap(arr => {
      for (elem <- arr) yield (elem.hashCode.toLong, elem)
    })
    //组合边 为了过滤 组合每个顶点都连接的边 不影响构建图
    val edges = ids.flatMap(arr => {
      val sortedArr = arr.sorted
      for (i <- 0 until sortedArr.length; j <- i + 1 until sortedArr.length) yield
        Edge(sortedArr(i).hashCode.toLong, sortedArr(j).hashCode.toLong, "")
    })

    val edgesres = edges.map(e=>(e,1)).reduceByKey(_+_).filter(t=>t._2 > 2).map(_._1)
    
    //获取T数据的idmp
    
    val idmpTdf = spark.read.parquet("user_profile/demodata/idmp/output/day01")
    val tvertices = idmpTdf.rdd.flatMap{
      case Row(id:Long, gid:Long) => Array((id,""), (gid,""))
    }
    val tedges = idmpTdf.rdd.map {
      case Row(id:Long, gid:Long) => Edge(id, gid, "")
    }

    val graph = Graph(vertices.union(tvertices), edgesres.union(tedges))
    val childrenGraph = graph.connectedComponents()
    //获取t+1 和t 的顶点对应的最小值
    val t1Vertices: VertexRDD[VertexId] = childrenGraph.vertices

    //修改t+1的id对应的最小值 为id在t日已存在 id对应最小值
    val idmp = idmpTdf.rdd.map{
      case Row(id:Long, gid:Long) => (id, gid)
    }.collectAsMap()
    val idmpbc = spark.sparkContext.broadcast(idmp)
    // 利用上日idmap调整当日计算结果
    val result = t1Vertices.groupBy(_._2).flatMap(t => {
      val nset = t._2.toMap.keySet

      val idmpdict = idmpbc.value
      val oset = idmpdict.keySet

      val intersectSet = nset.intersect(oset)
      var gid = t._1
      if (intersectSet != null && intersectSet.size > 0) {
        gid = idmpdict.get(intersectSet.head).get
      }
      nset.map(t => {

        (t, gid)
      })

    })
    result.toDF("id", "gid").coalesce(1).write.parquet("user_profile/demodata/idmp/output/day02")
    result.toDF("id","gid").show(100,false)
    spark.close()
  }
}
