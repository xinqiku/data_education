package cn.dyc.profile.idmp

import cn.dyc.util.{IdsExtractor, SparkUtil}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph}

/**
  * @program: data_education
  * @author: dyc
  * @create: 2019/09/16
  * @description:
  */
object TdayIdmp {
  def main(args: Array[String]): Unit = {
    //加载数据: 运营商 需求平台 竞价平台
    Logger.getLogger("org").setLevel(Level.WARN)
    val spark = SparkUtil.getSparkSession()
    import spark.implicits._
    val cmccPath = "user_profile/demodata/idmp/input/day01/cmcclog"
    val dspPath = "user_profile/demodata/idmp/input/day01/dsplog"
    val eventPath = "user_profile/demodata/idmp/input/day01/eventlog"


    val cmccrdd = IdsExtractor.processDemoIds(spark, cmccPath)
    val dsprdd = IdsExtractor.processDemoIds(spark, dspPath)
    val eventrdd = IdsExtractor.processDemoIds(spark, eventPath)

    val ids = cmccrdd.union(dsprdd).union(eventrdd)
    //获取顶点
    val vertices = ids.flatMap(arr => {
      for (elem <- arr) yield (elem.hashCode.toLong, elem)
    })
    //组合边 为了过滤 组合每个顶点都连接的边 不影响构建图
    val edges = ids.flatMap(arr => {
      val sortedArr = arr.sorted
      for (i <- 0 until sortedArr.length; j <- i + 1 until sortedArr.length) yield
        Edge(sortedArr(i).hashCode.toLong, sortedArr(j).hashCode.toLong, "")
    })

    val edgesres = edges.map(e=>(e,1)).reduceByKey(_+_).filter(t=>t._2 > 2).map(_._1)

    //画图
    val graph = Graph(vertices, edgesres)
    //获取最大连通子图
    val childrenGraph = graph.connectedComponents()
    //获取顶点和连通最小值
    val connectV = childrenGraph.vertices

    //获取属性和连通最小值

//    val verticesJoin = vertices.join(connectV)
//    val attrGid = verticesJoin.map(t=>(t._2._1, t._2._2))
    connectV.toDF("id", "gid").coalesce(1).write.parquet("user_profile/demodata/idmp/output/day01")


    spark.close()
  }
}
