package cn.dyc.crawler

import java.util

import org.apache.commons.io.IOUtils
import org.apache.http.Header
import org.apache.http.client.methods.{HttpGet, HttpUriRequest}
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.message.BasicHeader

/**
 * @date: 2019/9/15
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  用于理解啥叫爬虫
 */
object HttpClientDemo {

  def main(args: Array[String]): Unit = {

    val h1 = new BasicHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
    val h2 = new BasicHeader("Referer","Referer: https://www.baidu.com/link?url=dSvMAusHL1gefE2l8oxCjJaMu6cURl7_wF9tNX-GYzr0-Xy2EDwt1HX3IsBL_cU67AAQ4R0RkQygiD8yg0G7n_iimuC_1YM3n9l9XkHPP-VpkFaHwqyqFsFMZEnSAZU5")
    val h3 = new BasicHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0")

    val headers = new util.ArrayList[Header]()
    headers.add(h1)
    headers.add(h2)
    headers.add(h3)


    val client = HttpClientBuilder
      .create()
      .setDefaultHeaders(headers)
      //.setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0")
      .build()


    val url = "http://www.appchina.com/"

    val request = new HttpGet(url)

    val response = client.execute(request)

    val content = response.getEntity.getContent

    val lines = IOUtils.readLines(content)

    import scala.collection.JavaConversions._
    lines.foreach(println _)

    client.close()
  }

}
