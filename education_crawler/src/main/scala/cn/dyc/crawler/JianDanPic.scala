package cn.dyc.crawler

import java.io.FileOutputStream

import org.apache.commons.io.{FileUtils, IOUtils}
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

/**
  * @date: 2019/9/16
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 煎蛋网妹子图爬取
  */
object JianDanPic {

  def main(args: Array[String]): Unit = {

    val client = HttpClientBuilder.create().build()



    //      val page = s"http://jandan.net/ooxx/page-${i}#comments"
    val page = s"http://zhainanba.net/category/zhainanfuli/jinrimeizi"
    val doc = Jsoup.connect(page).get()
    val elements = doc.select("h2>a[target*=_blank]")


    import scala.collection.JavaConversions._
    for (atag <- elements) {
      println(s"正在爬取第 页......................................")
      val href = atag.attr("href")
      val document = Jsoup.connect(href).get()
      val elementsjpg = document.select("img[class*=alignnone]")
      for(jpg <- elementsjpg) {
        val hrefjpg = jpg.attr("src")
        val picName = hrefjpg.substring(hrefjpg.lastIndexOf("/") + 1)
        println(s"正在爬取 ${picName} 图片----------")
        val get = new HttpGet(hrefjpg)
        val in = client.execute(get).getEntity.getContent
        val out = new FileOutputStream(s"d:/pics/${picName}")
        IOUtils.copy(in, out)

        IOUtils.closeQuietly(in)
        IOUtils.closeQuietly(out)
        Thread.sleep(100)
      }

    }
    //      val elements = doc.getElementsByClass("view_img_link")

    //      println(str)
    //      val elements = doc.select("div.row>*")
    //      var isRow = true;
    //      for( element <- elements){
    //
    //        val elementsTable = new Elements();
    //        if(!element.hasClass("author")){
    //          isRow = false;
    //        }
    //        if(!isRow && element.hasClass("bbb")){
    //          elementsTable.add(element);
    //        }
    //      }
    //      val elements = doc.select("div:has(strong:contains(来个c))+div>p>a.view_img_link")
    //      import scala.collection.JavaConversions._
    //
    //      for (atag <- elements) {
    //          val href = atag.attr("href")
    //          val picName = href.substring(href.lastIndexOf("/") + 1)
    //          println(s"正在爬取 ${picName} 图片----------")
    //          val get = new HttpGet("http:" + href)
    //          val in = client.execute(get).getEntity.getContent
    //          val out = new FileOutputStream(s"d:/pics/${picName}")
    //          IOUtils.copy(in, out)
    //
    //          IOUtils.closeQuietly(in)
    //          IOUtils.closeQuietly(out)
    //          Thread.sleep(1000)
    //      }
    //    }

    client.close()

  }

}
